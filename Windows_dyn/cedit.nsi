  
  !include LogicLib.nsh
  # EDIT THIS
  # dynamic
  
  
  !define COMPANY_NAME "Ceicer IT"
  !define PRODUCT_PUBLISHER "Ceicer IT"
##!define PROGRAMMERS_WEBSITE "Website"
  !define APPICON "myapp.ico"
  
  !define INSTALL_DIR_NAME "cedit"
  !define INSTALL_DIR_SUB_NAME1 "bearer"
  !define INSTALL_DIR_SUB_NAME2 "iconengines"
  !define INSTALL_DIR_SUB_NAME3 "imageformats"
  !define INSTALL_DIR_SUB_NAME4 "platforms"
  !define INSTALL_DIR_SUB_NAME5 "printsupport"
  !define INSTALL_DIR_SUB_NAME6 "styles"
  !define INSTALL_DIR_SUB_NAME7 "help"
  
  !define DISPLAY_NAME "CEdit"
  
  !define PROG_NAME "cedit"
  !define VER_NAME "0.0.1"
  
  
  !define BRANDING_TEXT "Ceicer IT"
  !define BGGRADIENT "009463" #off
  
  !define LICENSE_DATA "license.txt"
  
  Icon "icons\install.ico"
  UninstallIcon "icons\uninstall.ico"
  


  !include LogicLib.nsh
  !define LOCALE_SNATIVELANGNAME '0x4' ;System native language name

  Var /GLOBAL stopp




LicenseForceSelection off ;radiobuttons | checkbox

ShowInstDetails hide
ShowUnInstDetails hide
SetCompress off

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
LoadLanguageFile "${NSISDIR}\Contrib\Language files\Swedish.nlf"





Page license
 

   


; License headline
;LicenseText "You must agree to this license before installing."
; Text file containing the license
LicenseData "${LICENSE_DATA}"


; The name of the installer
Name "${DISPLAY_NAME} ${VER_NAME}"

; The file to write
# OutFile "install-${VER_NAME}.exe"
  OutFile "install.exe"




BrandingText "${BRANDING_TEXT}"
BGGradient "${BGGRADIENT}"

; The default installation directory
    # 64-bit
     InstallDir $PROGRAMFILES64\${INSTALL_DIR_NAME}
    # 32-bit
    # InstallDir $PROGRAMFILES32\${INSTALL_DIR_NAME}



; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\${DISPLAY_NAME}" "Install_Dir"

;--------------------------------

; Pages

Page components
Page directory
Page instfiles


UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------



; The stuff to install
Section "Required"




  SectionIn RO
System::Call 'kernel32::GetLocaleInfoA(i 1024, i ${LOCALE_SNATIVELANGNAME}, t .r1, i ${NSIS_MAX_STRLEN}) i r0'
 ${If} $stopp == "1"
	${If} $1 == "svenska"
	messageBox MB_OK "Du m�ste avinstallera den nuvarande versionen$\ninnan du kan installera en ny!$\nInstallationen avbryts."
	Quit
	${Else}
	messageBox MB_OK "You must uninstall the current version $\nbefore you can install a new one!$\nThe installation is interrupted."
	Quit
	${EndIf}
  ${EndIf}
  

  
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"

  SetShellVarContext all # i startmenyn f�r "all users", default "current"
  
  
  ; Put file there
    File ${PROG_NAME}-${VER_NAME}.exe
   ##File "${PROGRAMMERS_WEBSITE}.URL"
	File ${LICENSE_DATA}
	File ${APPICON}
	File translation_sv_SE.qm
    File version_history.en_US
	File version_history.sv_SE
	
	
	File D3Dcompiler_47.dll
	File libEGL.dll
	File libGLESV2.dll
	File opengl32sw.dll
	File Qt5Core.dll
	File Qt5Gui.dll
	File Qt5Network.dll
	File Qt5Svg.dll
	File Qt5Widgets.dll
	File Qt5PrintSupport.dll
	
	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME1}
    File ${INSTALL_DIR_SUB_NAME1}\qgenericbearer.dll
    	
	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME2}
    File ${INSTALL_DIR_SUB_NAME2}\qsvgicon.dll
	
	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME3}
    File ${INSTALL_DIR_SUB_NAME3}\qgif.dll
	File ${INSTALL_DIR_SUB_NAME3}\qicns.dll
	File ${INSTALL_DIR_SUB_NAME3}\qico.dll
	File ${INSTALL_DIR_SUB_NAME3}\qjpeg.dll
	File ${INSTALL_DIR_SUB_NAME3}\qsvg.dll
	File ${INSTALL_DIR_SUB_NAME3}\qtga.dll
	File ${INSTALL_DIR_SUB_NAME3}\qtiff.dll
	File ${INSTALL_DIR_SUB_NAME3}\qwbmp.dll
	File ${INSTALL_DIR_SUB_NAME3}\qwebp.dll
	
	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME4}
	File ${INSTALL_DIR_SUB_NAME4}\qwindows.dll

	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME5}
	File ${INSTALL_DIR_SUB_NAME5}\windowsprintersupport.dll
	
	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME6}
	File ${INSTALL_DIR_SUB_NAME6}\qwindowsvistastyle.dll
	
	SetOutPath $INSTDIR\${INSTALL_DIR_SUB_NAME7}
	File ${INSTALL_DIR_SUB_NAME7}\help.html
    File ${INSTALL_DIR_SUB_NAME7}\help_eng.html
	File ${INSTALL_DIR_SUB_NAME7}\stilmall.css
	
	SetOutPath $INSTDIR
    
   
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\${INSTALL_DIR_NAME} "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "DisplayName" "${DISPLAY_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "DisplayVersion" "${VER_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "Publisher" "${COMPANY_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\${DISPLAY_NAME}"
   CreateShortCut "$SMPROGRAMS\${DISPLAY_NAME}\${DISPLAY_NAME} ${VER_NAME}.lnk" "$INSTDIR\${PROG_NAME}-${VER_NAME}.exe" "" "$INSTDIR\${PROG_NAME}-${VER_NAME}.exe" 0
   CreateShortCut "$SMPROGRAMS\${DISPLAY_NAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
   ##CreateShortCut "$SMPROGRAMS\${DISPLAY_NAME}\${PROGRAMMERS_WEBSITE}.lnk" "$INSTDIR\${PROGRAMMERS_WEBSITE}.URL" "" "$INSTDIR\${APPICON}" 0
   
SectionEnd
#Section "Desktop Shortcut" 
#SetShellVarContext current

    #SetOutPath "$INSTDIR"
    #CreateShortcut "$DESKTOP\${DISPLAY_NAME}-${VER_NAME}.lnk" "$INSTDIR\${PROG_NAME}-${VER_NAME}.exe" "" "$INSTDIR\${APPICON}" 
#SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"




SetShellVarContext all

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}"
  DeleteRegKey HKLM SOFTWARE\${INSTALL_DIR_NAME}

  ; Remove files and uninstaller
  Delete $INSTDIR\${PROG_NAME}-${VER_NAME}.exe
  Delete $INSTDIR\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME1}\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME2}\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME3}\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME4}\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME5}\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME6}\*.*
  Delete $INSTDIR\${INSTALL_DIR_SUB_NAME7}\*.*


  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\${DISPLAY_NAME}\*.*"
  Delete "$DESKTOP\${DISPLAY_NAME}.lnk"
  Delete "$PROFILE\.${INSTALL_DIR_NAME}\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\${DISPLAY_NAME}"
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME1}
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME2}
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME3}
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME4}
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME5}
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME6}
  RMDir $INSTDIR\${INSTALL_DIR_SUB_NAME7}
  RMDir "$INSTDIR"
  RMDir "$PROFILE\.${INSTALL_DIR_NAME}"
  

SectionEnd







Function .onInit


System::Call 'kernel32::GetLocaleInfoA(i 1024, i ${LOCALE_SNATIVELANGNAME}, t .r1, i ${NSIS_MAX_STRLEN}) i r0'





  ReadRegStr $R0 HKLM \
  "Software\Microsoft\Windows\CurrentVersion\Uninstall\${DISPLAY_NAME}" \
  "UninstallString"
  StrCmp $R0 "" done
 
  ${If} $1 == "svenska"
  MessageBox MB_OKCANCEL|MB_ICONINFORMATION \
  " En tidigare version av ${DISPLAY_NAME} �r redan installerad. $\n$\nKlicka $\"OK$\" f�r att ta bort \
  den tidigare versionen.$\nKlicka $\"Avbryt$\" f�r att avbryta uppgraderingen." \
  IDOK uninst
  Abort
  ${Else}
  MessageBox MB_OKCANCEL|MB_ICONINFORMATION \
  " A previous version of ${DISPLAY_NAME} is already installed. $\n$\nClick $\"OK$\" to remove the \
  previous version or $\"Cancel$\" to cancel this upgrade." \
  IDOK uninst
  Abort
  ${EndIf}
  
 
;Run the uninstaller
uninst:

    ClearErrors
    ;ExecWait '$R0 _?=$INSTDIR' ;Do not copy the uninstaller to a temp file
	ExecWait '$R0 _?=$INSTDIR' $stopp
   ;Exec $INSTDIR\uninst.exe ; instead of the ExecWait line

  IfErrors done
 
  done:
 
FunctionEnd


Function .onInstSuccess

System::Call 'kernel32::GetLocaleInfoA(i 1024, i ${LOCALE_SNATIVELANGNAME}, t .r1, i ${NSIS_MAX_STRLEN}) i r0'

${If} $1 == "svenska"
	  MessageBox MB_YESNO "Vill du l�sa om f�rb�ttringarna$\nsedan f�rra versionen?" IDNO NoReadme2
      Exec "notepad.exe version_history.sv_SE" 
    NoReadme2:
${Else}
	  MessageBox MB_YESNO "Want to read about improvements$\nsince the previous version?" IDNO NoReadme
      Exec "notepad.exe version_history.en_US" 
    NoReadme:
${EndIf}
 
 FunctionEnd
 
 
