//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>/



#include "mainwindow.h"
#include "system.h"

System::System(QObject *parent) : QObject(parent)
{
}
QString System::om()
{
    const QString BESKRIVNING = tr("A simple text editor.");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "nothing").toString();
    settings.endGroup();
    QString applicationHomepage = "";

    if(value == "en_US" || value == "nothing")
        applicationHomepage = APPLICATION_HOMEPAGE_ENG;
    else if(value == "sv_SE")
        applicationHomepage = APPLICATION_HOMEPAGE;
    else
        applicationHomepage = HOMEPAGE;

    //  const QString elGR = tr("Many thanks to ")+"<a href=\""+elGR_TRANSLATOR+"\">"+elGR_TRANSLATOR +"</a>"+tr(" for the Greek translation.");
    //  const QString deDE = tr("Many thanks to ")+ deDE_TRANSLATOR +tr(" for the German translation.");
    const QString license = tr("This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version ") + LICENCE_VERSION + tr(" of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.");
    const QString programmet = "<h1><font color=\"#009900\">" PROG_NAME " " VERSION "</h1>" + BESKRIVNING + "<br>";
    QString auther;
    int currentyear = QDate::currentDate().year();
    QString scurrentyear;
    scurrentyear.setNum(currentyear);
    //const QString screationyear(CREATION_YEAR);

    if(CREATION_YEAR != scurrentyear)
        auther = "<br><font color=\"green\" size=\"3\"><b>Copyright &copy; " CREATION_YEAR " - " + scurrentyear + " " PROGRAMMER_NAME "<br><a href=\"" + applicationHomepage + "\">" + applicationHomepage + "</a><br>" PROGRAMMER_EMAIL "<br>" + tr("Phone: ") + PROGRAMMER_PHONE "</b></font><br><br>";
    else
        auther = "<br><font color=\"green\" size=\"3\"><b>Copyright &copy; " CREATION_YEAR " " PROGRAMMER_NAME "<br><a href=\"" + applicationHomepage + "\">" + applicationHomepage + "</a><br>" PROGRAMMER_EMAIL "<br>" + tr("Phone: ") + PROGRAMMER_PHONE "</b></font><br><br>";

    return programmet + auther + license + "<br><br>";
}
QString System::getSystem()
{
    QString v = om();
    v += tr("This program uses Qt version ") + QT_VERSION_STR + tr(" running on ");
    v += QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + " (" + QSysInfo::currentCpuArchitecture() + ").<br>";
    v += "<br>" PROG_NAME " " VERSION + tr(" was created ") + BUILD_DATE_TIME "<br>" + tr("by a computer with") + " " + QSysInfo::buildAbi() + ".";
    v += "<br>";
    // write GCC version
#if defined   __GNUC__ && !defined __clang__
#ifdef __MINGW32__
#define COMPILER "MinGW GCC"
#else
#define COMPILER "GCC"
#endif
    v += (QString(tr("Compiled by") + " %1 %2.%3.%4%5").arg(COMPILER).arg(__GNUC__).arg(__GNUC_MINOR__).arg(__GNUC_PATCHLEVEL__).arg("."));
#elif defined __clang__
    v += (QString(tr("Compiled by") +  " %1 %2.%3.%4%5").arg("Clang").arg(__clang_major__).arg(__clang_minor__).arg(__clang_patchlevel__).arg("."));
    // MSVC version
#elif defined _MSC_VER
#define COMPILER "MSVC++"
    QString tmp = QString::number(_MSC_VER);
    QString version;
    QString full_version = tr("Full version number ") + QString::number(_MSC_FULL_VER);

    switch(_MSC_VER) {
        case 1500:
            version = "9.0 (Visual Studio 2008)<br>" + full_version;
            break;

        case 1600:
            version = "10.0 (Visual Studio 2010)<br>" + full_version;
            break;

        case 1700:
            version = "11.0 (Visual Studio 2012)<br>" + full_version;
            break;

        case 1800:
            version = "12.0 (Visual Studio 2013)<br>" + full_version;
            break;

        case 1900:
            version = "14.0 (Visual Studio 2015)<br>" + full_version;
            break;

        case 1910:
            version = "15.0 (Visual Studio 2017)<br>" + full_version;
            break;

        case 1912:
            version = "15.5 (Visual Studio 2017)<br>" + full_version;
            break;

        case 1914:
            version = "15.7 (Visual Studio 2017)<br>" + full_version;
            break;

        case 1915:
            version = "15.8 (Visual Studio 2017)<br>" + full_version;
            break;

        default:
            version = tr("Unknown version") + "<br>" + full_version;
            break;
    }

    v += (QString(tr("Compiled by") +  " %1 %2%3").arg(COMPILER).arg(version).arg("."));
#else
    v += tr("Unknown compiler.");
#endif
    v += "<br>";
    return v;
}
