//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          zsyncUpdateAppImage
//          Copyright (C) 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "updatedialog.h"
#include "ui_updatedialog.h"
#include "mainwindow.h"

UpdateDialog::UpdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);
    setWindowTitle(PROG_NAME " " VERSION);
    this->setFixedSize(323, 77);
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}
