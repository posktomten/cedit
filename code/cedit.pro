#-------------------------------------------------
#
# Project created by QtCreator 2018-03-22T11:38:41
#
#-------------------------------------------------

#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          CEdit
#//          Copyright (C) 2018 - 2020 Ingemar Ceicer
#//          https://gitlab.com/posktomten/cedit
#//          ic_0002 (at) ceicer (dot) com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

# libstdc++.so.6 and libgcc_s.so.1 compatible with ubuntu 14.04
# linux-g++ | linux-g++-64 | linux-g++-32 {
#    QMAKE_CXX = g++-4.8
#    QMAKE_CC = gcc-4.8
#}

    QT += core gui network printsupport
    CONFIG += c++11


#    linux-g++ | win32 {
#        CONFIG += precompile_header
#   }

    greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

    TARGET = cedit
    TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
     DEFINES += QT_DEPRECATED_WARNINGS
   # DEFINES += QT_FATAL_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        isuppdated.cpp \
        main.cpp \
        mainwindow.cpp \
        mainwindow_priv_slots.cpp \
        system.cpp \
        visa.cpp \
        tools.cpp

#linux-g++ | win32 {
#        PRECOMPILED_HEADER = stable.h
#}


HEADERS += \
        mainwindow.h \
        system.h \
        visa.h \
        tools.h \
        stable.h

FORMS += \
        mainwindow.ui \
        visa.ui \
        tools.ui


LIBS += "-L../lib" # Path to lib
LIBS += -lcheckforupdates  # Very unclear, the library file is named "libupdate.so" (dynamic) or "libupdate.a" (static)
LIBS += -lzsyncupdateappimage # Very unclear, the library file is named "libcheckforupdates.so"unix
INCLUDEPATH += ../include



TRANSLATIONS += i18n/translation_xx_XX.ts \
                i18n/translation_sv_SE.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DESTDIR="../build-executable"
unix:UI_DIR = ../code
win32:UI_DIR = ../code

RESOURCES += \
resources.qrc

RC_FILE = myapp.rc


