//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    auto *a = new QApplication(argc, argv);
    //Font
    QString fontPath = ":/fonts/Ubuntu-Regular.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QFont f("Ubuntu", FONT_SIZE);
        a->setFont(f);
    }

    // End Font
//    const QString INSTALL_DIR = QDir::toNativeSeparators(a.applicationDirPath() + "/");
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    const QString value = settings.value("lang", "nothing").toString();
    settings.endGroup();
    QTranslator translator;
    const QString locale = QLocale::system().name();

    if(value  == "nothing") {
        if(translator.load(":/i18n/translation_" + locale)) {
            a->installTranslator(&translator);
        } else {
            if(locale != "en_US")
                QMessageBox::information(nullptr, PROG_NAME " " VERSION, PROG_NAME " is unfortunately not yet translated into your language, the program will start with English menus.");
        }
    } else {
        if(translator.load(":/i18n/translation_" + value))
            a->installTranslator(&translator);
    }

    //  MainWindow w;
    //  w.show();
    auto *w = new MainWindow;
    w->show();
    return a->exec();
}
