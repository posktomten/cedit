//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef FRAME_H
#define FRAME_H
#include "stable.h"
#include <QFontComboBox>
namespace Ui
{
class Tools;

}

class Tools : public QFrame
{
    Q_OBJECT


public:
    explicit Tools(QWidget *parent = nullptr);
    ~Tools();
protected:
    friend class MainWindow;
    static QString findPath(int i);


private:
    Ui::Tools *ui;



private slots:
    void setStartConfig();
    void setEndConfig();
    void defaultOpenPathChanged(int index);
    void defaultOpenFallbackPathChanged(int index);
    void defaultSavePathChanged(int index);
    void defaultSaveFallbackPathChanged(int index);
    void loadFiles(int state);
    void setFont2(QFont *font);
    void monofaceFont(int state);
    void fontSize(int state);
    void wordwrap();
    void noWordwrap();



};

#endif // FRAME_H
