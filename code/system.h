//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef SYSTEM_H
#define SYSTEM_H
#include "stable.h"

class System : public QObject
{
    Q_OBJECT
public:
    explicit System(QObject *parent = nullptr);
    QString getSystem();

private:
    QString om();

signals:

public slots:
};

#endif // SYSTEM_H
