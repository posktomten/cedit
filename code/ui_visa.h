/********************************************************************************
** Form generated from reading UI file 'visa.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VISA_H
#define UI_VISA_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>

QT_BEGIN_NAMESPACE

class Ui_Visa
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QTextBrowser *tbVisa;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbClose;

    void setupUi(QDialog *Visa)
    {
        if (Visa->objectName().isEmpty())
            Visa->setObjectName(QString::fromUtf8("Visa"));
        Visa->setWindowModality(Qt::NonModal);
        Visa->resize(380, 343);
        Visa->setContextMenuPolicy(Qt::DefaultContextMenu);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/mall.png"), QSize(), QIcon::Normal, QIcon::Off);
        Visa->setWindowIcon(icon);
        Visa->setStyleSheet(QString::fromUtf8(""));
        Visa->setSizeGripEnabled(true);
        Visa->setModal(false);
        gridLayout = new QGridLayout(Visa);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        tbVisa = new QTextBrowser(Visa);
        tbVisa->setObjectName(QString::fromUtf8("tbVisa"));
        tbVisa->setContextMenuPolicy(Qt::DefaultContextMenu);
        tbVisa->setAcceptDrops(false);
        tbVisa->setStyleSheet(QString::fromUtf8(""));
        tbVisa->setFrameShape(QFrame::StyledPanel);
        tbVisa->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(tbVisa);


        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pbClose = new QPushButton(Visa);
        pbClose->setObjectName(QString::fromUtf8("pbClose"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pbClose->sizePolicy().hasHeightForWidth());
        pbClose->setSizePolicy(sizePolicy);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/dialog-close.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbClose->setIcon(icon1);
        pbClose->setFlat(false);

        horizontalLayout->addWidget(pbClose);


        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);


        retranslateUi(Visa);

        QMetaObject::connectSlotsByName(Visa);
    } // setupUi

    void retranslateUi(QDialog *Visa)
    {
        Visa->setWindowTitle(QCoreApplication::translate("Visa", "Dialog", nullptr));
        pbClose->setText(QCoreApplication::translate("Visa", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Visa: public Ui_Visa {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VISA_H
