<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="55"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="69"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="77"/>
        <source>&amp;Tools</source>
        <oldsource>&amp;Tool</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="87"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="98"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="106"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="133"/>
        <location filename="../mainwindow.ui" line="136"/>
        <location filename="../mainwindow.cpp" line="52"/>
        <location filename="../mainwindow.cpp" line="53"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="139"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="148"/>
        <location filename="../mainwindow.ui" line="151"/>
        <location filename="../mainwindow.ui" line="154"/>
        <location filename="../mainwindow.cpp" line="46"/>
        <location filename="../mainwindow.cpp" line="47"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="157"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="169"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="207"/>
        <location filename="../mainwindow.ui" line="210"/>
        <location filename="../mainwindow.ui" line="213"/>
        <location filename="../mainwindow.cpp" line="118"/>
        <location filename="../mainwindow.cpp" line="119"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="222"/>
        <location filename="../mainwindow.cpp" line="130"/>
        <location filename="../mainwindow.cpp" line="131"/>
        <source>Help Online...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="225"/>
        <location filename="../mainwindow.ui" line="228"/>
        <source>Online help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="237"/>
        <location filename="../mainwindow.ui" line="240"/>
        <location filename="../mainwindow.ui" line="243"/>
        <source>Check for updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="252"/>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.ui" line="258"/>
        <source>Version History...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="275"/>
        <location filename="../mainwindow.ui" line="278"/>
        <location filename="../mainwindow.ui" line="281"/>
        <location filename="../mainwindow.cpp" line="124"/>
        <location filename="../mainwindow.cpp" line="125"/>
        <source>License...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="286"/>
        <location filename="../mainwindow.ui" line="289"/>
        <location filename="../mainwindow.ui" line="292"/>
        <source>Clear settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="331"/>
        <location filename="../mainwindow.cpp" line="133"/>
        <location filename="../mainwindow.cpp" line="134"/>
        <source>Help Offline...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="334"/>
        <location filename="../mainwindow.ui" line="337"/>
        <source>Offline help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="346"/>
        <location filename="../mainwindow.cpp" line="143"/>
        <location filename="../mainwindow.cpp" line="144"/>
        <location filename="../mainwindow.cpp" line="145"/>
        <source>Translate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="355"/>
        <location filename="../mainwindow.cpp" line="58"/>
        <location filename="../mainwindow.cpp" line="59"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="370"/>
        <location filename="../mainwindow.cpp" line="104"/>
        <location filename="../mainwindow.cpp" line="105"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="373"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="382"/>
        <location filename="../mainwindow.cpp" line="107"/>
        <location filename="../mainwindow.cpp" line="108"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="385"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="390"/>
        <location filename="../mainwindow.cpp" line="111"/>
        <location filename="../mainwindow.cpp" line="112"/>
        <source>Word Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="395"/>
        <location filename="../mainwindow.cpp" line="114"/>
        <location filename="../mainwindow.cpp" line="115"/>
        <source>No Word Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="404"/>
        <location filename="../mainwindow.cpp" line="73"/>
        <location filename="../mainwindow.cpp" line="74"/>
        <source>Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="416"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="425"/>
        <location filename="../mainwindow.cpp" line="86"/>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>Print to pdf...</source>
        <oldsource>Print to PDF...</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="439"/>
        <source>Close all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="444"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="166"/>
        <location filename="../mainwindow.cpp" line="90"/>
        <location filename="../mainwindow.cpp" line="91"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="174"/>
        <location filename="../mainwindow.ui" line="177"/>
        <location filename="../mainwindow.ui" line="180"/>
        <location filename="../mainwindow.cpp" line="101"/>
        <location filename="../mainwindow.cpp" line="102"/>
        <source>Copy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="189"/>
        <location filename="../mainwindow.cpp" line="137"/>
        <location filename="../mainwindow.cpp" line="138"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="198"/>
        <location filename="../mainwindow.cpp" line="140"/>
        <location filename="../mainwindow.cpp" line="141"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <source>Search for updates when the program starts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="301"/>
        <location filename="../mainwindow.ui" line="304"/>
        <location filename="../mainwindow.ui" line="307"/>
        <location filename="../mainwindow.cpp" line="64"/>
        <location filename="../mainwindow.cpp" line="65"/>
        <source>Save as...</source>
        <oldsource>Save...</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="316"/>
        <location filename="../mainwindow.ui" line="319"/>
        <location filename="../mainwindow.ui" line="322"/>
        <location filename="../mainwindow.cpp" line="154"/>
        <location filename="../mainwindow.cpp" line="155"/>
        <source>Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="79"/>
        <location filename="../mainwindow.cpp" line="80"/>
        <source>Print (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="97"/>
        <location filename="../mainwindow.cpp" line="98"/>
        <source>Close current tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="121"/>
        <location filename="../mainwindow.cpp" line="122"/>
        <source>Update...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="127"/>
        <location filename="../mainwindow.cpp" line="128"/>
        <source>Version history...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="148"/>
        <location filename="../mainwindow.cpp" line="149"/>
        <source>Update at the start of the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <location filename="../mainwindow.cpp" line="152"/>
        <source>Remove all settings and exit the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="156"/>
        <location filename="../mainwindow.cpp" line="157"/>
        <source>If you click, the program is updated. If no update is available, the update process will still be performed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="233"/>
        <location filename="../mainwindow_priv_slots.cpp" line="574"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="237"/>
        <location filename="../mainwindow_priv_slots.cpp" line="578"/>
        <source>Download a new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="239"/>
        <location filename="../mainwindow_priv_slots.cpp" line="580"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update component&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="289"/>
        <location filename="../mainwindow_priv_slots.cpp" line="233"/>
        <location filename="../mainwindow_priv_slots.cpp" line="310"/>
        <location filename="../mainwindow_priv_slots.cpp" line="370"/>
        <source>New </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="328"/>
        <location filename="../mainwindow.cpp" line="383"/>
        <source>There are files that have not been saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="329"/>
        <location filename="../mainwindow.cpp" line="384"/>
        <source>Do you want to save the changes before closing the application?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="104"/>
        <location filename="../mainwindow_priv_slots.cpp" line="182"/>
        <source>new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="108"/>
        <source>Save file to pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="195"/>
        <source> Print Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="269"/>
        <source>Open file/files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="411"/>
        <location filename="../mainwindow_priv_slots.cpp" line="499"/>
        <source>Unable to save file, check your file permissions! </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="465"/>
        <source>Save file/files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="597"/>
        <location filename="../mainwindow_priv_slots.cpp" line="608"/>
        <source>Unable to find &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="622"/>
        <location filename="../mainwindow_priv_slots.cpp" line="650"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="623"/>
        <location filename="../mainwindow_priv_slots.cpp" line="651"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="624"/>
        <location filename="../mainwindow_priv_slots.cpp" line="652"/>
        <location filename="../mainwindow_priv_slots.cpp" line="696"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="694"/>
        <source>All program settings will be deleted. The program will shut down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="695"/>
        <source>Delete all settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="775"/>
        <source>The document has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow_priv_slots.cpp" line="776"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>System</name>
    <message>
        <location filename="../system.cpp" line="29"/>
        <source>A simple text editor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="46"/>
        <source>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="46"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="55"/>
        <location filename="../system.cpp" line="57"/>
        <source>Phone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="64"/>
        <source>This program uses Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="64"/>
        <source> running on </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="66"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="66"/>
        <source>by a computer with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="75"/>
        <location filename="../system.cpp" line="77"/>
        <location filename="../system.cpp" line="127"/>
        <source>Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="83"/>
        <source>Full version number </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="123"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../system.cpp" line="129"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools</name>
    <message>
        <location filename="../tools.ui" line="17"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="47"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="59"/>
        <source>Default for New/Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="71"/>
        <source>Word Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="84"/>
        <source>No Word Wrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="100"/>
        <source>Open/Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="121"/>
        <source>Default open path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a name=&quot;result_box&quot;/&gt;Preset option to open&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="204"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a name=&quot;result_box&quot;/&gt;Reserve option to open if default options fail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="223"/>
        <location filename="../tools.ui" line="360"/>
        <source>Fallback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="236"/>
        <source>Load files from last session</source>
        <oldsource>Load files from the last session</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="261"/>
        <source>Default save path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="282"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a name=&quot;result_box&quot;/&gt;Preset option to save&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="341"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a name=&quot;result_box&quot;/&gt;Reserve option to save if default options fail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="369"/>
        <location filename="../tools.ui" line="433"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="391"/>
        <source>Only show Monospace fonts</source>
        <oldsource>Show only Monospace fonts</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="414"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.ui" line="457"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="207"/>
        <location filename="../tools.cpp" line="239"/>
        <source>Last used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="208"/>
        <location filename="../tools.cpp" line="228"/>
        <location filename="../tools.cpp" line="240"/>
        <location filename="../tools.cpp" line="260"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="209"/>
        <location filename="../tools.cpp" line="229"/>
        <location filename="../tools.cpp" line="241"/>
        <location filename="../tools.cpp" line="261"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="210"/>
        <location filename="../tools.cpp" line="230"/>
        <location filename="../tools.cpp" line="242"/>
        <location filename="../tools.cpp" line="262"/>
        <source>Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="211"/>
        <location filename="../tools.cpp" line="231"/>
        <location filename="../tools.cpp" line="243"/>
        <location filename="../tools.cpp" line="263"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="212"/>
        <location filename="../tools.cpp" line="232"/>
        <location filename="../tools.cpp" line="244"/>
        <location filename="../tools.cpp" line="264"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="213"/>
        <location filename="../tools.cpp" line="233"/>
        <location filename="../tools.cpp" line="245"/>
        <location filename="../tools.cpp" line="265"/>
        <source>Movies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="214"/>
        <location filename="../tools.cpp" line="234"/>
        <location filename="../tools.cpp" line="246"/>
        <location filename="../tools.cpp" line="266"/>
        <source>Pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="215"/>
        <location filename="../tools.cpp" line="235"/>
        <location filename="../tools.cpp" line="247"/>
        <location filename="../tools.cpp" line="267"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="216"/>
        <location filename="../tools.cpp" line="248"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="227"/>
        <location filename="../tools.cpp" line="259"/>
        <source>No fallback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="307"/>
        <source>Open Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools.cpp" line="360"/>
        <source>Save Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Visa</name>
    <message>
        <location filename="../visa.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../visa.ui" line="70"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
