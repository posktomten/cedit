﻿//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

/*
setStartConfig()
setEndConfig(QCloseEvent *event)
setEndConfig()
startaom()
void closeEvent(QCloseEvent *event);
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "update.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //this->setStyle(QStyleFactory::create("fusion"));
    //qDebug() << this->style();
    this->setAttribute(Qt::WA_DeleteOnClose, true);
    /* draganddrop */
    setAcceptDrops(true);
    ui->setupUi(this);
    setStartConfig();
    // Signals to private slots
    // File Ny
    connect(ui->actionNew, &QAction::triggered, [this]() {
        ny();
    });
    ui->actionNew->setStatusTip(tr("New..."));
    ui->actionNew->setToolTip(tr("New..."));
    //File Open
    connect(ui->actionOpen, &QAction::triggered, [this]() {
        open();
    });
    ui->actionOpen->setStatusTip(tr("Open..."));
    ui->actionOpen->setToolTip(tr("Open..."));
    //File Save
    connect(ui->actionSave, &QAction::triggered, [this]() {
        save();
    });
    ui->actionSave->setStatusTip(tr("Save"));
    ui->actionSave->setToolTip(tr("Save"));
    //File Save as...
    connect(ui->actionSaveAs, &QAction::triggered, [this]() {
        saveAs();
    });
    ui->actionSaveAs->setStatusTip(tr("Save as..."));
    ui->actionSaveAs->setToolTip(tr("Save as..."));
    connect(ui->actionExit, &QAction::triggered, [this]() {
        setEndConfig();
    });
    //File Print...
    connect(ui->actionPrint, &QAction::triggered, [this]() {
        print();
    });
    ui->actionPrint->setStatusTip(tr("Print..."));
    ui->actionPrint->setToolTip(tr("Print..."));
    //File Print
    connect(ui->actionPrintDirect, &QAction::triggered, [this]() {
        printDirect();
    });
    ui->actionPrintDirect->setStatusTip(tr("Print (") + QPrinterInfo::defaultPrinterName() + ")");
    ui->actionPrintDirect->setToolTip(tr("Print (") + QPrinterInfo::defaultPrinterName() + ")");
    //
    //File Print PDF
    connect(ui->actionPrintPDF, &QAction::triggered, [this]() {
        printPDF();
    });
    ui->actionPrintPDF->setStatusTip(tr("Print to pdf..."));
    ui->actionPrintPDF->setToolTip(tr("Print to pdf..."));
    //
    // File exit
    ui->actionExit->setStatusTip(tr("Exit"));
    ui->actionExit->setToolTip(tr("Exit"));
    //
    // File closeTab
    connect(ui->actionCloseTab, &QAction::triggered, [this]() {
        closeTab();
    });
    ui->actionCloseTab->setStatusTip(tr("Close current tab"));
    ui->actionCloseTab->setToolTip(tr("Close current tab"));
    // Edit
    connect(ui->actionCopyPath, &QAction::triggered, [this] { copyPath(); });
    ui->actionCopyPath->setStatusTip(tr("Copy Path"));
    ui->actionCopyPath->setToolTip(tr("Copy Path"));
    connect(ui->actionRedo, &QAction::triggered, [this] { redo(); });
    ui->actionRedo->setStatusTip(tr("Redo"));
    ui->actionRedo->setToolTip(tr("Redo"));
    connect(ui->actionUndo, &QAction::triggered, [this] { undo(); });
    ui->actionUndo->setStatusTip(tr("Undo"));
    ui->actionUndo->setToolTip(tr("Undo"));
    // View
    connect(ui->actionWordWrap, &QAction::triggered, [this] { wordWrap(); });
    ui->actionWordWrap->setStatusTip(tr("Word Wrap"));
    ui->actionWordWrap->setToolTip(tr("Word Wrap"));
    connect(ui->actionNoWordWrap, &QAction::triggered, [this] { noWordWrap(); });
    ui->actionNoWordWrap->setStatusTip(tr("No Word Wrap"));
    ui->actionNoWordWrap->setToolTip(tr("No Word Wrap"));
    // Help
    connect(ui->actionAbout, &QAction::triggered, [this] { about(); });
    ui->actionAbout->setStatusTip(tr("About..."));
    ui->actionAbout->setToolTip(tr("About..."));
    connect(ui->actionUpdate, &QAction::triggered, [this] { update(); });
    ui->actionUpdate->setStatusTip(tr("Update..."));
    ui->actionUpdate->setToolTip(tr("Update..."));
    connect(ui->actionLicense, &QAction::triggered, [this] { license(); });
    ui->actionLicense->setStatusTip(tr("License..."));
    ui->actionLicense->setToolTip(tr("License..."));
    connect(ui->actionVersionhistory, &QAction::triggered, [this] { versionhistory(); });
    ui->actionVersionhistory->setStatusTip(tr("Version history..."));
    ui->actionVersionhistory->setToolTip(tr("Version history..."));
    connect(ui->actionHelpOnline, &QAction::triggered, [this] { helpOnline(); });
    ui->actionHelpOnline->setStatusTip(tr("Help Online..."));
    ui->actionHelpOnline->setToolTip(tr("Help Online..."));
    connect(ui->actionHelpOffline, &QAction::triggered, [this] { helpOffline(); });
    ui->actionHelpOffline->setStatusTip(tr("Help Offline..."));
    ui->actionHelpOffline->setToolTip(tr("Help Offline..."));
    // Language
    connect(ui->actionEnglish, &QAction::triggered, [this] { english(); });
    ui->actionEnglish->setStatusTip(tr("English"));
    ui->actionEnglish->setToolTip(tr("English"));
    connect(ui->actionSwedish, &QAction::triggered, [this] { swedish(); });
    ui->actionSwedish->setStatusTip(tr("Swedish"));
    ui->actionSwedish->setToolTip(tr("Swedish"));
    connect(ui->actionTranslate, &QAction::triggered, [this] { translate(); });
    ui->actionTranslate->setText(tr("Translate") + " " + PROG_NAME + "...");
    ui->actionTranslate->setStatusTip(tr("Translate") + " " + PROG_NAME + "...");
    ui->actionTranslate->setToolTip(tr("Translate") + " " + PROG_NAME + "...");
    // Tools
    connect(ui->actionUpdateProgramstart, &QAction::triggered, [this] { updateProgramstart(); });
    ui->actionUpdateProgramstart->setStatusTip(tr("Update at the start of the program"));
    ui->actionUpdateProgramstart->setToolTip(tr("Update at the start of the program"));
    connect(ui->actionClearSettings, &QAction::triggered, [this] { removeSettings(); });
    ui->actionClearSettings->setStatusTip(tr("Remove all settings and exit the program"));
    ui->actionClearSettings->setToolTip(tr("Remove all settings and exit the program"));
    connect(ui->actionSettings, &QAction::triggered, [this] { settings(); });
    ui->actionSettings->setStatusTip(tr("Settings..."));
    ui->actionSettings->setToolTip(tr("Settings..."));
    ui->actionDoUpdate->setStatusTip(tr("If you click, the program is updated. If no update is available, the update process will still be performed."));
    ui->actionDoUpdate->setToolTip(tr("If you click, the program is updated. If no update is available, the update process will still be performed."));
    connect(ui->actionDoUpdate, &QAction::triggered, [this]() ->void {
        // QDialog class
        ud = new UpdateDialog;
        ud->show();
        // QObject class
        Update *up = new Update;
        connect(up, &Update::isUpdated, this, &MainWindow::isUpdated);

        // Pointer is sent so that "ud" can delete
        up->doUpdate(ZSYNC_ARG_1, ZSYNC_ARG_2, PROG_NAME, ud);

    });
// MainToolBar
// exit
    ui->mainToolBar->addAction(ui->actionExit);
// new
    ui->mainToolBar->addAction(ui->actionNew);
// open
    ui->mainToolBar->addAction(ui->actionOpen);
// save
    ui->mainToolBar->addAction(ui->actionSave);
// print
    ui->mainToolBar->addAction(ui->actionPrintDirect);
// redo
    ui->mainToolBar->addAction(ui->actionRedo);
// undo
    ui->mainToolBar->addAction(ui->actionUndo);
    /****
    *****/
// Click the tab
// tabBarClicked
// currentChanged
    connect(ui->tabWidget, &QTabWidget::tabBarClicked, [this](int index) {
        tabClicked(index);
    });
// Close tab
    connect(ui->tabWidget, &QTabWidget::tabCloseRequested, [this](int index) {
        closeRequested(index);
    });
//  const int currentTabIndex = ui->tabWidget->currentIndex();
//  auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(currentTabIndex));
//  connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
//      textChanged();
//  });
}
MainWindow::~MainWindow()
{
    setEndConfig();
}
void MainWindow::setStartConfig()
{
    ui->tabWidget->setTabsClosable(true);
    this->setWindowTitle(PROG_NAME " " VERSION);
    QScreen *screen = QGuiApplication::primaryScreen();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("MainWindow");
    this->resize(settings.value("size", QSize(600, 600)).toSize());
    QPoint p = settings.value("pos", QPoint(100, 100)).toPoint();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QPoint  screenSavedSize = settings.value("screenSavedSize", QPoint(0, 0)).toPoint();
    settings.endGroup();

    if(screenSavedSize == screenSize)
        this->move(p);
    else
        this->move(100, 100);

    settings.beginGroup("Tools");
    QString updateAtStart = settings.value("UpdateAtStart", "1").toString();
    settings.endGroup();

    if(updateAtStart == "1") {
#ifdef Q_OS_LINUX
        QString *updateinstructions = new QString(tr("Select \"Tools\", \"Update\" to update."));
#endif
#ifdef Q_OS_WIN
#ifdef portable
        QString *updateinstructions = new QString(tr("Download a new") + " <a href=\"" DOWNLOAD_PATH "\"> portable</a>");
#endif
        QString *updateinstructions = new QString(tr("Select \"Tools\", \"Maintenance Tool\" and \"Update component\"."));
#endif
        auto *c = new CheckForUpdates;
        c->checkOnStart(PROG_NAME, VERSION, VERSION_PATH, *updateinstructions);
        ui->actionUpdateProgramstart->setChecked(true);
    } else
        ui->actionUpdateProgramstart->setChecked(false);

    /* Open all files */
    QString filename;
    settings.beginGroup("Files");
    const QStringList childKeys = settings.childKeys();
    settings.endGroup();

    if(!childKeys.empty()) {
        settings.beginGroup("Files");

        foreach(const QString & childKey, childKeys) {
            filename = settings.value(childKey).toString();

            if(QFileInfo::exists(filename)) {
                tab << filename;
                modified << false;
            }
        }

        settings.endGroup();
    }

    settings.beginGroup("WordWrap");
    QString wordwrap = settings.value("wordwrap", "No").toString();
    settings.endGroup();
    settings.beginGroup("Font");
    const QString ceditFont = settings.value("font", MONOFACE_FONT).toString();
    const int fontsize = settings.value("fontSize", DEFAULT_FONT_SIZE).toInt();
    settings.endGroup();
    QFont font(ceditFont);
    font.setPointSize(fontsize);

    for(int i = 0; i < tab.size(); i++) {
        filename = tab.at(i);
        QFile inputFile(filename);
        auto *plainedit = new QPlainTextEdit;
        plainedit->setFont(font);

        if(wordwrap == "No")
            plainedit->setWordWrapMode(QTextOption::NoWrap);
        else
            plainedit->setWordWrapMode(QTextOption::WordWrap);

        ui->tabWidget->insertTab(i, plainedit, tr("New ") + QString::number(i));
        inputFile.open(QIODevice::Text | QIODevice::ReadOnly);
        QString content = "";

        while(!inputFile.atEnd())
            content.append(inputFile.readLine());

        plainedit->setPlainText(content);
        inputFile.close();
        QFileInfo fi(filename);
        QString fil = fi.fileName();
        ui->tabWidget->setTabText(i, fil);
        ui->tabWidget->setCurrentIndex(i);
        ui->tabWidget->setTabToolTip(i, filename);
    }

    QStringList argument = QApplication::arguments();

    if(argument.size() > 1) {
        argument.removeFirst();
        const QStringList *args = new QStringList(argument);
        open(args);
    }

    int a = ui->tabWidget->currentIndex();
    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(a));
    connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
        textChanged();
    });
    // ui->tabWidget->setCurrentIndex(a);
}
// private:
bool MainWindow::setEndConfig(QCloseEvent * event)
{
    bool allfalse = modified.contains(true);

    if(allfalse) {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle(PROG_NAME " " VERSION);
        msgBox.setText(tr("There are files that have not been saved."));
        msgBox.setInformativeText(tr("Do you want to save the changes before closing the application?"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();

        switch(ret) {
            case QMessageBox::No:
                // No was clicked
                event->accept();
                break;

            case QMessageBox::Yes:
                // Yes was clicked
                event->ignore();
                return true;

            default:
                // should never be reached
                break;
        }
    }

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("MainWindow");
    settings.setValue("screenSavedSize", screenSize);
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
    settings.beginGroup("ToolsWindow");
    int openFilesOnStart = settings.value("loadFilesOnStart", 0).toInt();
    settings.endGroup();
    settings.beginGroup("Files");
    settings.remove("");

    if(openFilesOnStart > 0) {
        for(int i = 0; i < tab.size(); i++)
            settings.setValue(QString::number(i), tab.at(i));
    }

    settings.endGroup();
    event->accept();
    return false;
}
bool MainWindow::setEndConfig()
{
    bool allfalse = modified.contains(true);

    if(allfalse) {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle(PROG_NAME " " VERSION);
        msgBox.setText(tr("There are files that have not been saved."));
        msgBox.setInformativeText(tr("Do you want to save the changes before closing the application?"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        int ret = msgBox.exec();

        switch(ret) {
            case QMessageBox::No:
                // No was clicked
                modified.clear();
                break;

            case QMessageBox::Yes:
                // Yes was clicked
                return true;

            default:
                // should never be reached
                break;
        }
    }

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("MainWindow");
    settings.setValue("screenSavedSize", screenSize);
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
    settings.beginGroup("ToolsWindow");
    int openFilesOnStart = settings.value("loadFilesOnStart", 0).toInt();
    settings.endGroup();
    settings.beginGroup("Files");
    settings.remove("");

    if(openFilesOnStart > 0) {
        for(int i = 0; i < tab.size(); i++)
            settings.setValue(QString::number(i), tab.at(i));
    }

    settings.endGroup();
    close();
    return false;
}
void MainWindow::startaom()
{
    const QString EXECUTE = QDir::toNativeSeparators(
                                QCoreApplication::applicationDirPath() + "/" +
                                QFileInfo(QCoreApplication::applicationFilePath()).fileName());
    QProcess p;
    p.setProgram(EXECUTE);
    p.startDetached();
    close();
}
void MainWindow::closeEvent(QCloseEvent * event)
{
    setEndConfig(event);
}
void MainWindow::dropEvent(QDropEvent * ev)
{
    ev->accept();
    QStringList surls;

    foreach(QUrl url, ev->mimeData()->urls()) {
        surls << url.toLocalFile();
    }

    open(&surls);
}
void MainWindow::dragEnterEvent(QDragEnterEvent * ev)
{
    ev->accept();
}
