/********************************************************************************
** Form generated from reading UI file 'tools.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TOOLS_H
#define UI_TOOLS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Tools
{
public:
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tabGeneral;
    QGroupBox *groupBoxWordwrap;
    QRadioButton *rbWordWrap;
    QRadioButton *rbNoWordWrap;
    QWidget *tabOpenSave;
    QVBoxLayout *verticalLayout;
    QGroupBox *gbOpen;
    QComboBox *comboOpen;
    QLabel *lblOpenPath;
    QComboBox *comboOpenFallback;
    QLabel *lblFallback_o;
    QCheckBox *chbLoadFiles;
    QGroupBox *gbSave;
    QComboBox *comboSave;
    QLabel *lblSavePath;
    QComboBox *comboSaveFallback;
    QLabel *lblFallback_s;
    QWidget *tabFont;
    QFontComboBox *fontComboBox;
    QCheckBox *chbMonospace;
    QSpinBox *spbFontSize;
    QLabel *lblFontSize;
    QLabel *lblFont;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pbClose;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_3;

    void setupUi(QFrame *Tools)
    {
        if (Tools->objectName().isEmpty())
            Tools->setObjectName(QString::fromUtf8("Tools"));
        Tools->setWindowModality(Qt::ApplicationModal);
        Tools->resize(1096, 460);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/mall.png"), QSize(), QIcon::Normal, QIcon::Off);
        Tools->setWindowIcon(icon);
        Tools->setStyleSheet(QString::fromUtf8(""));
        verticalLayout_5 = new QVBoxLayout(Tools);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        tabWidget = new QTabWidget(Tools);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setElideMode(Qt::ElideNone);
        tabGeneral = new QWidget();
        tabGeneral->setObjectName(QString::fromUtf8("tabGeneral"));
        groupBoxWordwrap = new QGroupBox(tabGeneral);
        groupBoxWordwrap->setObjectName(QString::fromUtf8("groupBoxWordwrap"));
        groupBoxWordwrap->setGeometry(QRect(10, 20, 181, 91));
        rbWordWrap = new QRadioButton(groupBoxWordwrap);
        rbWordWrap->setObjectName(QString::fromUtf8("rbWordWrap"));
        rbWordWrap->setGeometry(QRect(10, 30, 151, 23));
        rbNoWordWrap = new QRadioButton(groupBoxWordwrap);
        rbNoWordWrap->setObjectName(QString::fromUtf8("rbNoWordWrap"));
        rbNoWordWrap->setGeometry(QRect(10, 60, 161, 23));
        tabWidget->addTab(tabGeneral, QString());
        tabOpenSave = new QWidget();
        tabOpenSave->setObjectName(QString::fromUtf8("tabOpenSave"));
        tabOpenSave->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(tabOpenSave);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gbOpen = new QGroupBox(tabOpenSave);
        gbOpen->setObjectName(QString::fromUtf8("gbOpen"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(gbOpen->sizePolicy().hasHeightForWidth());
        gbOpen->setSizePolicy(sizePolicy);
        gbOpen->setAutoFillBackground(false);
        gbOpen->setStyleSheet(QString::fromUtf8("padding-left:5px;\n"
"padding-right: 10px;\n"
"\n"
""));
        gbOpen->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        gbOpen->setFlat(false);
        comboOpen = new QComboBox(gbOpen);
        comboOpen->setObjectName(QString::fromUtf8("comboOpen"));
        comboOpen->setGeometry(QRect(10, 30, 141, 24));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboOpen->sizePolicy().hasHeightForWidth());
        comboOpen->setSizePolicy(sizePolicy1);
        comboOpen->setToolTipDuration(-1);
        lblOpenPath = new QLabel(gbOpen);
        lblOpenPath->setObjectName(QString::fromUtf8("lblOpenPath"));
        lblOpenPath->setGeometry(QRect(10, 60, 701, 61));
        sizePolicy.setHeightForWidth(lblOpenPath->sizePolicy().hasHeightForWidth());
        lblOpenPath->setSizePolicy(sizePolicy);
        lblOpenPath->setToolTipDuration(-1);
        lblOpenPath->setStyleSheet(QString::fromUtf8("padding-left:0;"));
        lblOpenPath->setTextFormat(Qt::PlainText);
        lblOpenPath->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lblOpenPath->setWordWrap(true);
        lblOpenPath->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        comboOpenFallback = new QComboBox(gbOpen);
        comboOpenFallback->setObjectName(QString::fromUtf8("comboOpenFallback"));
        comboOpenFallback->setGeometry(QRect(180, 30, 141, 24));
        sizePolicy1.setHeightForWidth(comboOpenFallback->sizePolicy().hasHeightForWidth());
        comboOpenFallback->setSizePolicy(sizePolicy1);
        comboOpenFallback->setToolTipDuration(-1);
        lblFallback_o = new QLabel(gbOpen);
        lblFallback_o->setObjectName(QString::fromUtf8("lblFallback_o"));
        lblFallback_o->setGeometry(QRect(180, 0, 121, 16));
        lblFallback_o->setStyleSheet(QString::fromUtf8("padding-left:0;"));
        chbLoadFiles = new QCheckBox(gbOpen);
        chbLoadFiles->setObjectName(QString::fromUtf8("chbLoadFiles"));
        chbLoadFiles->setGeometry(QRect(350, 30, 381, 23));
        comboOpenFallback->raise();
        lblOpenPath->raise();
        lblFallback_o->raise();
        comboOpen->raise();
        chbLoadFiles->raise();

        verticalLayout->addWidget(gbOpen);

        gbSave = new QGroupBox(tabOpenSave);
        gbSave->setObjectName(QString::fromUtf8("gbSave"));
        sizePolicy.setHeightForWidth(gbSave->sizePolicy().hasHeightForWidth());
        gbSave->setSizePolicy(sizePolicy);
        gbSave->setStyleSheet(QString::fromUtf8("padding-left:5px;\n"
"padding-right: 10px;\n"
"\n"
""));
        gbSave->setFlat(false);
        comboSave = new QComboBox(gbSave);
        comboSave->setObjectName(QString::fromUtf8("comboSave"));
        comboSave->setGeometry(QRect(10, 30, 141, 24));
        sizePolicy1.setHeightForWidth(comboSave->sizePolicy().hasHeightForWidth());
        comboSave->setSizePolicy(sizePolicy1);
        comboSave->setToolTipDuration(-1);
        lblSavePath = new QLabel(gbSave);
        lblSavePath->setObjectName(QString::fromUtf8("lblSavePath"));
        lblSavePath->setGeometry(QRect(10, 60, 711, 61));
        sizePolicy.setHeightForWidth(lblSavePath->sizePolicy().hasHeightForWidth());
        lblSavePath->setSizePolicy(sizePolicy);
        lblSavePath->setAutoFillBackground(false);
        lblSavePath->setStyleSheet(QString::fromUtf8("padding-left:0;"));
        lblSavePath->setTextFormat(Qt::PlainText);
        lblSavePath->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lblSavePath->setWordWrap(true);
        lblSavePath->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);
        comboSaveFallback = new QComboBox(gbSave);
        comboSaveFallback->setObjectName(QString::fromUtf8("comboSaveFallback"));
        comboSaveFallback->setGeometry(QRect(180, 30, 151, 24));
        sizePolicy1.setHeightForWidth(comboSaveFallback->sizePolicy().hasHeightForWidth());
        comboSaveFallback->setSizePolicy(sizePolicy1);
        comboSaveFallback->setToolTipDuration(-1);
        lblFallback_s = new QLabel(gbSave);
        lblFallback_s->setObjectName(QString::fromUtf8("lblFallback_s"));
        lblFallback_s->setGeometry(QRect(180, 0, 131, 16));
        lblFallback_s->setStyleSheet(QString::fromUtf8("padding-left:0;"));

        verticalLayout->addWidget(gbSave);

        tabWidget->addTab(tabOpenSave, QString());
        tabFont = new QWidget();
        tabFont->setObjectName(QString::fromUtf8("tabFont"));
        fontComboBox = new QFontComboBox(tabFont);
        fontComboBox->setObjectName(QString::fromUtf8("fontComboBox"));
        fontComboBox->setGeometry(QRect(10, 30, 261, 25));
        chbMonospace = new QCheckBox(tabFont);
        chbMonospace->setObjectName(QString::fromUtf8("chbMonospace"));
        chbMonospace->setGeometry(QRect(290, 30, 251, 31));
        spbFontSize = new QSpinBox(tabFont);
        spbFontSize->setObjectName(QString::fromUtf8("spbFontSize"));
        spbFontSize->setGeometry(QRect(10, 90, 61, 22));
        lblFontSize = new QLabel(tabFont);
        lblFontSize->setObjectName(QString::fromUtf8("lblFontSize"));
        lblFontSize->setGeometry(QRect(10, 70, 91, 16));
        lblFont = new QLabel(tabFont);
        lblFont->setObjectName(QString::fromUtf8("lblFont"));
        lblFont->setGeometry(QRect(10, 10, 71, 16));
        lblFont->setLayoutDirection(Qt::LeftToRight);
        lblFont->setFrameShape(QFrame::NoFrame);
        tabWidget->addTab(tabFont, QString());

        horizontalLayout->addWidget(tabWidget);


        verticalLayout_2->addLayout(horizontalLayout);


        verticalLayout_5->addLayout(verticalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pbClose = new QPushButton(Tools);
        pbClose->setObjectName(QString::fromUtf8("pbClose"));
        pbClose->setMaximumSize(QSize(16777215, 16777215));
        pbClose->setStyleSheet(QString::fromUtf8(""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/dialog-close.png"), QSize(), QIcon::Normal, QIcon::Off);
        pbClose->setIcon(icon1);

        horizontalLayout_3->addWidget(pbClose);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_5->addLayout(horizontalLayout_3);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));

        verticalLayout_5->addLayout(verticalLayout_3);


        retranslateUi(Tools);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Tools);
    } // setupUi

    void retranslateUi(QFrame *Tools)
    {
        Tools->setWindowTitle(QCoreApplication::translate("Tools", "Tools", nullptr));
        groupBoxWordwrap->setTitle(QCoreApplication::translate("Tools", "Default for New/Open", nullptr));
        rbWordWrap->setText(QCoreApplication::translate("Tools", "Word Wrap", nullptr));
        rbNoWordWrap->setText(QCoreApplication::translate("Tools", "No Word Wrap", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabGeneral), QCoreApplication::translate("Tools", "General", nullptr));
#if QT_CONFIG(tooltip)
        tabOpenSave->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        tabOpenSave->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
        gbOpen->setTitle(QCoreApplication::translate("Tools", "Default open path", nullptr));
#if QT_CONFIG(tooltip)
        comboOpen->setToolTip(QCoreApplication::translate("Tools", "<html><head/><body><p><a name=\"result_box\"/>Preset option to open</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        lblOpenPath->setText(QString());
#if QT_CONFIG(tooltip)
        comboOpenFallback->setToolTip(QCoreApplication::translate("Tools", "<html><head/><body><p><a name=\"result_box\"/>Reserve option to open if default options fail</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        lblFallback_o->setText(QCoreApplication::translate("Tools", "Fallback", nullptr));
        chbLoadFiles->setText(QCoreApplication::translate("Tools", "Load files from last session", nullptr));
        gbSave->setTitle(QCoreApplication::translate("Tools", "Default save path", nullptr));
#if QT_CONFIG(tooltip)
        comboSave->setToolTip(QCoreApplication::translate("Tools", "<html><head/><body><p><a name=\"result_box\"/>Preset option to save</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        lblSavePath->setText(QString());
#if QT_CONFIG(tooltip)
        comboSaveFallback->setToolTip(QCoreApplication::translate("Tools", "<html><head/><body><p><a name=\"result_box\"/>Reserve option to save if default options fail</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        lblFallback_s->setText(QCoreApplication::translate("Tools", "Fallback", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabOpenSave), QCoreApplication::translate("Tools", "Open/Save", nullptr));
        chbMonospace->setText(QCoreApplication::translate("Tools", "Only show Monospace fonts", nullptr));
        lblFontSize->setText(QCoreApplication::translate("Tools", "Font Size", nullptr));
        lblFont->setText(QCoreApplication::translate("Tools", "Font", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tabFont), QCoreApplication::translate("Tools", "Font", nullptr));
        pbClose->setText(QCoreApplication::translate("Tools", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Tools: public Ui_Tools {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TOOLS_H
