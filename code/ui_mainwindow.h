/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionNew;
    QAction *actionExit;
    QAction *actionCopyPath;
    QAction *actionEnglish;
    QAction *actionSwedish;
    QAction *actionAbout;
    QAction *actionHelpOnline;
    QAction *actionUpdate;
    QAction *actionVersionhistory;
    QAction *actionUpdateProgramstart;
    QAction *actionLicense;
    QAction *actionClearSettings;
    QAction *actionSaveAs;
    QAction *actionSettings;
    QAction *actionHelpOffline;
    QAction *actionTranslate;
    QAction *actionSave;
    QAction *actionRedo;
    QAction *actionUndo;
    QAction *actionWordWrap;
    QAction *actionNoWordWrap;
    QAction *actionPrint;
    QAction *actionPrintDirect;
    QAction *actionPrintPDF;
    QAction *actionCloseTab;
    QAction *actionCloseAll;
    QAction *actionDoUpdate;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuEdit;
    QMenu *menuTools;
    QMenu *menuHelp;
    QMenu *menuLanguage;
    QMenu *menuView;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(406, 318);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/mall.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/document-open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionNew = new QAction(MainWindow);
        actionNew->setObjectName(QString::fromUtf8("actionNew"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/document-new.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNew->setIcon(icon2);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/application-exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon3);
        actionCopyPath = new QAction(MainWindow);
        actionCopyPath->setObjectName(QString::fromUtf8("actionCopyPath"));
        actionEnglish = new QAction(MainWindow);
        actionEnglish->setObjectName(QString::fromUtf8("actionEnglish"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/english.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEnglish->setIcon(icon4);
        actionSwedish = new QAction(MainWindow);
        actionSwedish->setObjectName(QString::fromUtf8("actionSwedish"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/swedish.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSwedish->setIcon(icon5);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/help-about.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAbout->setIcon(icon6);
        actionHelpOnline = new QAction(MainWindow);
        actionHelpOnline->setObjectName(QString::fromUtf8("actionHelpOnline"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/help-contents.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionHelpOnline->setIcon(icon7);
        actionUpdate = new QAction(MainWindow);
        actionUpdate->setObjectName(QString::fromUtf8("actionUpdate"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/update.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUpdate->setIcon(icon8);
        actionVersionhistory = new QAction(MainWindow);
        actionVersionhistory->setObjectName(QString::fromUtf8("actionVersionhistory"));
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/versionhistory.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionVersionhistory->setIcon(icon9);
        actionUpdateProgramstart = new QAction(MainWindow);
        actionUpdateProgramstart->setObjectName(QString::fromUtf8("actionUpdateProgramstart"));
        actionUpdateProgramstart->setCheckable(true);
        actionLicense = new QAction(MainWindow);
        actionLicense->setObjectName(QString::fromUtf8("actionLicense"));
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/license.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLicense->setIcon(icon10);
        actionClearSettings = new QAction(MainWindow);
        actionClearSettings->setObjectName(QString::fromUtf8("actionClearSettings"));
        actionSaveAs = new QAction(MainWindow);
        actionSaveAs->setObjectName(QString::fromUtf8("actionSaveAs"));
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/images/document-save-as.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSaveAs->setIcon(icon11);
        actionSettings = new QAction(MainWindow);
        actionSettings->setObjectName(QString::fromUtf8("actionSettings"));
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/images/preferences-system.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSettings->setIcon(icon12);
        actionHelpOffline = new QAction(MainWindow);
        actionHelpOffline->setObjectName(QString::fromUtf8("actionHelpOffline"));
        actionHelpOffline->setIcon(icon7);
        actionTranslate = new QAction(MainWindow);
        actionTranslate->setObjectName(QString::fromUtf8("actionTranslate"));
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/images/translate.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTranslate->setIcon(icon13);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/images/document-save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon14);
        actionSave->setShortcutContext(Qt::WindowShortcut);
        actionRedo = new QAction(MainWindow);
        actionRedo->setObjectName(QString::fromUtf8("actionRedo"));
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/images/edit-redo.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRedo->setIcon(icon15);
        actionUndo = new QAction(MainWindow);
        actionUndo->setObjectName(QString::fromUtf8("actionUndo"));
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/images/edit-undo.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUndo->setIcon(icon16);
        actionWordWrap = new QAction(MainWindow);
        actionWordWrap->setObjectName(QString::fromUtf8("actionWordWrap"));
        actionNoWordWrap = new QAction(MainWindow);
        actionNoWordWrap->setObjectName(QString::fromUtf8("actionNoWordWrap"));
        actionPrint = new QAction(MainWindow);
        actionPrint->setObjectName(QString::fromUtf8("actionPrint"));
        QIcon icon17;
        icon17.addFile(QString::fromUtf8(":/images/document-print.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrint->setIcon(icon17);
        actionPrintDirect = new QAction(MainWindow);
        actionPrintDirect->setObjectName(QString::fromUtf8("actionPrintDirect"));
        QIcon icon18;
        icon18.addFile(QString::fromUtf8(":/images/document-print-direct.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrintDirect->setIcon(icon18);
        actionPrintPDF = new QAction(MainWindow);
        actionPrintPDF->setObjectName(QString::fromUtf8("actionPrintPDF"));
        QIcon icon19;
        icon19.addFile(QString::fromUtf8(":/images/application-pdf.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrintPDF->setIcon(icon19);
        actionCloseTab = new QAction(MainWindow);
        actionCloseTab->setObjectName(QString::fromUtf8("actionCloseTab"));
        QIcon icon20;
        icon20.addFile(QString::fromUtf8(":/images/dialog-close.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCloseTab->setIcon(icon20);
        actionCloseAll = new QAction(MainWindow);
        actionCloseAll->setObjectName(QString::fromUtf8("actionCloseAll"));
        actionDoUpdate = new QAction(MainWindow);
        actionDoUpdate->setObjectName(QString::fromUtf8("actionDoUpdate"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setDocumentMode(true);

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 406, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuLanguage = new QMenu(menuBar);
        menuLanguage->setObjectName(QString::fromUtf8("menuLanguage"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuLanguage->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSaveAs);
        menuFile->addAction(actionPrint);
        menuFile->addAction(actionPrintDirect);
        menuFile->addAction(actionPrintPDF);
        menuFile->addAction(actionCloseTab);
        menuFile->addAction(actionExit);
        menuEdit->addAction(actionCopyPath);
        menuEdit->addAction(actionRedo);
        menuEdit->addAction(actionUndo);
        menuTools->addAction(actionUpdateProgramstart);
        menuTools->addAction(actionClearSettings);
        menuTools->addSeparator();
        menuTools->addAction(actionSettings);
        menuTools->addAction(actionDoUpdate);
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionHelpOnline);
        menuHelp->addAction(actionHelpOffline);
        menuHelp->addAction(actionUpdate);
        menuHelp->addAction(actionVersionhistory);
        menuHelp->addAction(actionLicense);
        menuLanguage->addAction(actionEnglish);
        menuLanguage->addAction(actionSwedish);
        menuLanguage->addAction(actionTranslate);
        menuView->addAction(actionWordWrap);
        menuView->addAction(actionNoWordWrap);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QString());
#if QT_CONFIG(statustip)
        MainWindow->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
        actionOpen->setText(QCoreApplication::translate("MainWindow", "Open...", nullptr));
        actionOpen->setIconText(QCoreApplication::translate("MainWindow", "Open...", nullptr));
#if QT_CONFIG(shortcut)
        actionOpen->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionNew->setText(QCoreApplication::translate("MainWindow", "New...", nullptr));
        actionNew->setIconText(QCoreApplication::translate("MainWindow", "New...", nullptr));
#if QT_CONFIG(tooltip)
        actionNew->setToolTip(QCoreApplication::translate("MainWindow", "New...", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionNew->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+N", nullptr));
#endif // QT_CONFIG(shortcut)
        actionExit->setText(QCoreApplication::translate("MainWindow", "Exit", nullptr));
#if QT_CONFIG(shortcut)
        actionExit->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCopyPath->setText(QCoreApplication::translate("MainWindow", "Copy Path", nullptr));
        actionCopyPath->setIconText(QCoreApplication::translate("MainWindow", "Copy Path", nullptr));
#if QT_CONFIG(tooltip)
        actionCopyPath->setToolTip(QCoreApplication::translate("MainWindow", "Copy Path", nullptr));
#endif // QT_CONFIG(tooltip)
        actionEnglish->setText(QCoreApplication::translate("MainWindow", "English", nullptr));
        actionSwedish->setText(QCoreApplication::translate("MainWindow", "Swedish", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About...", nullptr));
        actionAbout->setIconText(QCoreApplication::translate("MainWindow", "About...", nullptr));
#if QT_CONFIG(tooltip)
        actionAbout->setToolTip(QCoreApplication::translate("MainWindow", "About...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionHelpOnline->setText(QCoreApplication::translate("MainWindow", "Help Online...", nullptr));
        actionHelpOnline->setIconText(QCoreApplication::translate("MainWindow", "Online help...", nullptr));
#if QT_CONFIG(tooltip)
        actionHelpOnline->setToolTip(QCoreApplication::translate("MainWindow", "Online help...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionUpdate->setText(QCoreApplication::translate("MainWindow", "Check for updates...", nullptr));
        actionUpdate->setIconText(QCoreApplication::translate("MainWindow", "Check for updates...", nullptr));
#if QT_CONFIG(tooltip)
        actionUpdate->setToolTip(QCoreApplication::translate("MainWindow", "Check for updates...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionVersionhistory->setText(QCoreApplication::translate("MainWindow", "Version History...", nullptr));
        actionVersionhistory->setIconText(QCoreApplication::translate("MainWindow", "Version History...", nullptr));
#if QT_CONFIG(tooltip)
        actionVersionhistory->setToolTip(QCoreApplication::translate("MainWindow", "Version History...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionUpdateProgramstart->setText(QCoreApplication::translate("MainWindow", "Search for updates when the program starts", nullptr));
        actionLicense->setText(QCoreApplication::translate("MainWindow", "License...", nullptr));
        actionLicense->setIconText(QCoreApplication::translate("MainWindow", "License...", nullptr));
#if QT_CONFIG(tooltip)
        actionLicense->setToolTip(QCoreApplication::translate("MainWindow", "License...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionClearSettings->setText(QCoreApplication::translate("MainWindow", "Clear settings...", nullptr));
        actionClearSettings->setIconText(QCoreApplication::translate("MainWindow", "Clear settings...", nullptr));
#if QT_CONFIG(tooltip)
        actionClearSettings->setToolTip(QCoreApplication::translate("MainWindow", "Clear settings...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSaveAs->setText(QCoreApplication::translate("MainWindow", "Save as...", nullptr));
        actionSaveAs->setIconText(QCoreApplication::translate("MainWindow", "Save as...", nullptr));
#if QT_CONFIG(tooltip)
        actionSaveAs->setToolTip(QCoreApplication::translate("MainWindow", "Save as...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSettings->setText(QCoreApplication::translate("MainWindow", "Settings...", nullptr));
        actionSettings->setIconText(QCoreApplication::translate("MainWindow", "Settings...", nullptr));
#if QT_CONFIG(tooltip)
        actionSettings->setToolTip(QCoreApplication::translate("MainWindow", "Settings...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionHelpOffline->setText(QCoreApplication::translate("MainWindow", "Help Offline...", nullptr));
        actionHelpOffline->setIconText(QCoreApplication::translate("MainWindow", "Offline help...", nullptr));
#if QT_CONFIG(tooltip)
        actionHelpOffline->setToolTip(QCoreApplication::translate("MainWindow", "Offline help...", nullptr));
#endif // QT_CONFIG(tooltip)
        actionTranslate->setText(QCoreApplication::translate("MainWindow", "Translate", nullptr));
        actionSave->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
#if QT_CONFIG(shortcut)
        actionSave->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        actionRedo->setText(QCoreApplication::translate("MainWindow", "Redo", nullptr));
#if QT_CONFIG(shortcut)
        actionRedo->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Y", nullptr));
#endif // QT_CONFIG(shortcut)
        actionUndo->setText(QCoreApplication::translate("MainWindow", "Undo", nullptr));
#if QT_CONFIG(shortcut)
        actionUndo->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Z", nullptr));
#endif // QT_CONFIG(shortcut)
        actionWordWrap->setText(QCoreApplication::translate("MainWindow", "Word Wrap", nullptr));
        actionNoWordWrap->setText(QCoreApplication::translate("MainWindow", "No Word Wrap", nullptr));
        actionPrint->setText(QCoreApplication::translate("MainWindow", "Print...", nullptr));
#if QT_CONFIG(shortcut)
        actionPrint->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+P", nullptr));
#endif // QT_CONFIG(shortcut)
        actionPrintDirect->setText(QCoreApplication::translate("MainWindow", "Print", nullptr));
        actionPrintPDF->setText(QCoreApplication::translate("MainWindow", "Print to pdf...", nullptr));
        actionCloseTab->setText(QCoreApplication::translate("MainWindow", "Close", nullptr));
        actionCloseAll->setText(QCoreApplication::translate("MainWindow", "Close all", nullptr));
        actionDoUpdate->setText(QCoreApplication::translate("MainWindow", "Update", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "&File", nullptr));
        menuEdit->setTitle(QCoreApplication::translate("MainWindow", "&Edit", nullptr));
        menuTools->setTitle(QCoreApplication::translate("MainWindow", "&Tools", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("MainWindow", "&Help", nullptr));
        menuLanguage->setTitle(QCoreApplication::translate("MainWindow", "&Language", nullptr));
        menuView->setTitle(QCoreApplication::translate("MainWindow", "View", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
