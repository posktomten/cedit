﻿//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "tools.h"
#include "ui_tools.h"

#include "mainwindow.h"

Tools::Tools(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Tools)
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);
    ui->setupUi(this);
    // setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint); // Works on Windows, not Lubuntu Linux
    // Font
    /* QString fontPath = ":/fonts/fonts/Ubuntu-M.ttf";
     int id = QFontDatabase::addApplicationFont(fontPath);
     QString family = QFontDatabase::applicationFontFamilies(id).at(0);
     QFont default_font(family);
     default_font.setPointSize(DEFAULT_FONT_SIZE_SYSTEM);
     this->setFont(default_font);*/
    // End Font
    setStartConfig();
    connect(ui->pbClose, &QPushButton::clicked, [this] { setEndConfig(); });
    connect(ui->comboOpen, QOverload<int>::of(&QComboBox::activated), [this](int index) {
        defaultOpenPathChanged(index);
    });
    connect(ui->comboOpenFallback, QOverload<int>::of(&QComboBox::activated), [this](int index) {
        defaultOpenFallbackPathChanged(index);
    });
    connect(ui->comboSave, QOverload<int>::of(&QComboBox::activated), [this](int index) {
        defaultSavePathChanged(index);
    });
    connect(ui->comboSaveFallback, QOverload<int>::of(&QComboBox::activated), [this](int index) {
        defaultSaveFallbackPathChanged(index);
    });
    connect(ui->chbLoadFiles, &QCheckBox::stateChanged, [this](int state) {
        loadFiles(state);
    });
    connect(ui->fontComboBox, &QFontComboBox::currentFontChanged, [this](QFont font) {
        setFont2(&font);
    });
    /* connect(ui->fontComboBox, &QFontComboBox::currentTextChanged, [this](QFont font) {
         setFont2(font);
     }); */
    connect(ui->chbMonospace, &QCheckBox::stateChanged, [this](int state) {
        monofaceFont(state);
    });
    connect(ui->spbFontSize, QOverload<int>::of(&QSpinBox::valueChanged),
    [this](int state) {
        fontSize(state);
    });
    // Radio button Word Wrap
    connect(ui->rbWordWrap, &QAbstractButton::pressed, [this]() {
        wordwrap();
    });
    connect(ui->rbNoWordWrap, &QAbstractButton::pressed, [this]() {
        noWordwrap();
    });
}

void Tools::fontSize(int state)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Font");
    settings.setValue("fontSize", QString::number(state));
    settings.endGroup();
}

void Tools::wordwrap()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("WordWrap");
    settings.setValue("wordwrap", "Yes");
    settings.endGroup();
}
void Tools::noWordwrap()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("WordWrap");
    settings.setValue("wordwrap", "No");
    settings.endGroup();
}



void Tools::monofaceFont(int state)
{
    if(state > 0)
        ui->fontComboBox->setFontFilters(QFontComboBox::MonospacedFonts);
    else
        ui->fontComboBox->setFontFilters(QFontComboBox::AllFonts);
}
void Tools::setFont2(QFont *font)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Font");
    settings.setValue("font", font->toString());
    settings.endGroup();
}

void Tools::setStartConfig()
{
    this->setWindowTitle(PROG_NAME " " VERSION);
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    this->resize(settings.value("size", QSize(600, 400)).toSize());
    QPoint p = settings.value("pos", QPoint(100, 100)).toPoint();
    settings.endGroup();
    settings.beginGroup("MainWindow");
    QPoint  screenSavedSize = settings.value("screenSavedSize", QPoint(0, 0)).toPoint();
    settings.endGroup();

    if(screenSavedSize == screenSize)
        this->move(p);
    else
        this->move(100, 100);

    // qDebug() << "screenSize" << screenSize;
    // qDebug() << "screenSavedSize" << screenSavedSize;
    QString tmp_path = QStandardPaths::locate(QStandardPaths::HomeLocation, QString(), QStandardPaths::LocateDirectory);
    QString visa = tmp_path;
    int langd1  = visa.length();

    if(visa.at(langd1 - 1) == "/") {
        visa.remove(langd1 - 1, 1);
    }

    tmp_path = visa;
    //
    // Open dialog
    settings.beginGroup("ToolsWindow");
    int path_type_o = settings.value("openPathType", 1).toInt(); // 1 home
    int path_type_fallback_o = settings.value("openFallbackPathType", 1).toInt(); // 1 Home
    QString path_o;

    if(path_type_o == 0 || path_type_o == 9) {
        path_o = settings.value("openPath", tmp_path).toString();
    } else {
        path_o = findPath(path_type_o);
    }

    settings.endGroup();
    //
    // END open
    //
    // Save dialog
    settings.beginGroup("ToolsWindow");
    int path_type_s = settings.value("savePathType", 1).toInt(); // 1 home
    int path_type_fallback_s = settings.value("saveFallbackPathType", 1).toInt(); // 1 Home
    int openFilesOnStart = settings.value("loadFilesOnStart", 0).toInt();
    QString path_s;

    if(path_type_s == 0 || path_type_s == 9) {
        path_s = settings.value("savePath", tmp_path).toString();
    } else {
        path_s = findPath(path_type_s);
    }

    settings.endGroup();
    //
    // END Save
    //

    if(openFilesOnStart > 0)
        ui->chbLoadFiles->setChecked(true);
    else
        ui->chbLoadFiles->setChecked(false);

    // combo box "Open, Save"
    /* Last used, 0
     * HomeLocation, Home, 1
     * DownloadLocation, Download, 2
     * DesktopLocation, Desktop, 3
     * DocumentsLocation, Documents, 4
     * MusicLocation, Music, 5
     * MoviesLocation, Movies, 6
     * PicturesLocation, Pictures, 7
     * TempLocation, Temp, 8
     * Custom, 9
     */
    // Open dialog
    ui->comboOpen->addItem(tr("Last used"));
    ui->comboOpen->addItem(tr("Home"));
    ui->comboOpen->addItem(tr("Download"));
    ui->comboOpen->addItem(tr("Desktop"));
    ui->comboOpen->addItem(tr("Documents"));
    ui->comboOpen->addItem(tr("Music"));
    ui->comboOpen->addItem(tr("Movies"));
    ui->comboOpen->addItem(tr("Pictures"));
    ui->comboOpen->addItem(tr("Temp"));
    ui->comboOpen->addItem(tr("Custom"));
    ui->comboOpen->setCurrentIndex(path_type_o);
    visa = path_o;
    int langd  = visa.length();

    if(visa.at(langd - 1) == "/") {
        visa.remove(langd - 1, 1);
    }

    ui->lblOpenPath->setText(QDir::toNativeSeparators(visa));
    // Open dialog fallback
    ui->comboOpenFallback->addItem(tr("No fallback"));// 0
    ui->comboOpenFallback->addItem(tr("Home"));// 1
    ui->comboOpenFallback->addItem(tr("Download"));// 2
    ui->comboOpenFallback->addItem(tr("Desktop"));// 3
    ui->comboOpenFallback->addItem(tr("Documents"));// 4
    ui->comboOpenFallback->addItem(tr("Music"));// 5
    ui->comboOpenFallback->addItem(tr("Movies"));// 6
    ui->comboOpenFallback->addItem(tr("Pictures"));// 7
    ui->comboOpenFallback->addItem(tr("Temp"));// 8
    ui->comboOpenFallback->setCurrentIndex(path_type_fallback_o);
    //
    // Save dialog
    ui->comboSave->addItem(tr("Last used"));
    ui->comboSave->addItem(tr("Home"));
    ui->comboSave->addItem(tr("Download"));
    ui->comboSave->addItem(tr("Desktop"));
    ui->comboSave->addItem(tr("Documents"));
    ui->comboSave->addItem(tr("Music"));
    ui->comboSave->addItem(tr("Movies"));
    ui->comboSave->addItem(tr("Pictures"));
    ui->comboSave->addItem(tr("Temp"));
    ui->comboSave->addItem(tr("Custom"));
    ui->comboSave->setCurrentIndex(path_type_s);
    visa = path_s;
    langd  = visa.length();

    if(visa.at(langd - 1) == "/") {
        visa.remove(langd - 1, 1);
    }

    ui->lblSavePath->setText(QDir::toNativeSeparators(visa));
    // Save dialog fallback
    ui->comboSaveFallback->addItem(tr("No fallback"));
    ui->comboSaveFallback->addItem(tr("Home"));
    ui->comboSaveFallback->addItem(tr("Download"));
    ui->comboSaveFallback->addItem(tr("Desktop"));
    ui->comboSaveFallback->addItem(tr("Documents"));
    ui->comboSaveFallback->addItem(tr("Music"));
    ui->comboSaveFallback->addItem(tr("Movies"));
    ui->comboSaveFallback->addItem(tr("Pictures"));
    ui->comboSaveFallback->addItem(tr("Temp"));
    ui->comboSaveFallback->setCurrentIndex(path_type_fallback_s);
    ui->spbFontSize->setMinimum(FONT_SIZE_MINIMUM);
    ui->spbFontSize->setMaximum(FONT_SIZE_MAXIMUM);
    settings.beginGroup("Font");
    const QString ceditFont = settings.value("font", MONOFACE_FONT).toString();
    const int fontSize = settings.value("fontSize", DEFAULT_FONT_SIZE).toInt();
    settings.endGroup();
    ui->fontComboBox->setCurrentFont(ceditFont);
    ui->spbFontSize->setValue(fontSize);
    ui->tabWidget->setCurrentIndex(0);
    settings.beginGroup("WordWrap");
    const QString wordwrap = settings.value("wordwrap", "No").toString();
    settings.endGroup();

    if(wordwrap == "No") {
        ui->rbNoWordWrap->setChecked(true);
        ui->rbWordWrap->setChecked(false);
    }

    if(wordwrap == "Yes") {
        ui->rbNoWordWrap->setChecked(false);
        ui->rbWordWrap->setChecked(true);
    }
}

void Tools::setEndConfig()
{
    delete this;
}

void Tools::defaultOpenPathChanged(int index)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");

    if(index != 0) {
        if(index == 9) {
            QString path = settings.value("openPath", "nothing").toString();
            QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                          path,
                          QFileDialog::ShowDirsOnly
                          | QFileDialog::DontResolveSymlinks);

            if(dir != "") {
                QString visa = dir;
                int langd  = visa.length();

                if(visa.at(langd - 1) == "/") {
                    visa.remove(langd - 1, 1);
                }

                settings.setValue("openPath", visa);
                settings.setValue("openPathType", 9);
                ui->lblOpenPath->setText(QDir::toNativeSeparators(visa));
            }
        } else {
            QString path = findPath(index);
            settings.setValue("openPath", "");
            settings.setValue("openPathType", index);
            QString visa = path;
            int langd  = visa.length();

            if(visa[langd - 1] == "/") {
                visa.remove(langd - 1, 1);
            }

            ui->lblOpenPath->setText(QDir::toNativeSeparators(visa));
        }
    } else if(index == 0) {
        settings.setValue("openPathType", 0);
    }

    settings.endGroup();
}
void Tools::defaultOpenFallbackPathChanged(int index)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    settings.setValue("openFallbackPathType", index);
    settings.endGroup();
}
void Tools::defaultSavePathChanged(int index)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");

    if(index != 0) {
        if(index == 9) {
            QString path = settings.value("savePath", "nothing").toString();
            QString dir = QFileDialog::getExistingDirectory(this, tr("Save Directory"),
                          path,
                          QFileDialog::ShowDirsOnly
                          | QFileDialog::DontResolveSymlinks);

            if(dir != "") {
                settings.setValue("savePathType", 9);
                QString visa = dir;
                int langd  = visa.length();

                if(visa.at(langd - 1) == "/") {
                    visa.remove(langd - 1, 1);
                }

                ui->lblSavePath->setText(QDir::toNativeSeparators(visa));
                settings.setValue("savePath", visa);
            }
        } else {
            QString path = findPath(index);
            settings.setValue("savePath", "");
            settings.setValue("savePathType", index);
            QString visa = path;
            int langd  = visa.length();

            if(visa[langd - 1] == "/") {
                visa.remove(langd - 1, 1);
            }

            ui->lblSavePath->setText(QDir::toNativeSeparators(visa));
        }
    } else if(index == 0) {
        settings.setValue("savePathType", 0);
    }

    settings.endGroup();
}
void Tools::defaultSaveFallbackPathChanged(int index)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    settings.setValue("saveFallbackPathType", index);
    settings.endGroup();
}
QString  Tools::findPath(int i)
{
    QString path;

    switch(i) {
        case 1:
            path = QStandardPaths::locate(QStandardPaths::HomeLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 2:
            path = QStandardPaths::locate(QStandardPaths::DownloadLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 3:
            path = QStandardPaths::locate(QStandardPaths::DesktopLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 4:
            path = QStandardPaths::locate(QStandardPaths::DocumentsLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 5:
            path = QStandardPaths::locate(QStandardPaths::MusicLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 6:
            path = QStandardPaths::locate(QStandardPaths::MoviesLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 7:
            path = QStandardPaths::locate(QStandardPaths::PicturesLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        case 8:
            path = QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory);
            return path;

        default:
            path = QStandardPaths::locate(QStandardPaths::HomeLocation, QString(), QStandardPaths::LocateDirectory);
            return path;
    }
}

void Tools::loadFiles(int state)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    settings.setValue("loadFilesOnStart", state);
    settings.endGroup();
}

Tools::~Tools()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
    delete ui;
}
