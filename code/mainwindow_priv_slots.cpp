//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
    private slots:

    void about();
    void update();
    void english();
    void swedish();
    void translate();
    void updateProgramstart();
    void license();
    void versionhistory();
    void removeSettings();
    void settings();
    void helpOnline();
    void helpOffline();
    void ny();
    void tabClicked(int index);
    void closeRequested(int index);
    void textChanged(int index);
    void copyPath();
    void print();
    private:


    void open();
    void open(const QStringList *filenames);
    void save();
    void saveAs();

*/
// File



void MainWindow::closeTab()
{
    int currentTab = ui->tabWidget->currentIndex();
    closeRequested(currentTab);
}

void MainWindow::printPDF()
{
    const int current = ui->tabWidget->currentIndex();
    /* qDebug() << "List of printers";
     QList<QPrinterInfo> printerList = QPrinterInfo::availablePrinters();

     foreach(QPrinterInfo printerInfo, printerList) {
         qDebug() << printerInfo.printerName();
     }*/

    if(current < 0)
        return;

    // PDF
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    int index = settings.value("savePathType", 1).toInt();
    settings.endGroup();
    QString path;

    if(index != 0 && index != 9) {
        path = Tools::findPath(index);
    } else {
        path = settings.value("savePath").toString();
    }

    const QFileInfo outputDir(path);

    if((!outputDir.exists()) || (!outputDir.isDir())) {
        int fallback_path_index = settings.value("saveFallbackPathType", 1).toInt(); // 1 Home

        if(fallback_path_index == 0)
            path = Tools::findPath(0);
        else
            path = Tools::findPath(fallback_path_index);
    }

    QFileInfo fi(tab.at(current));
    QString fil = fi.fileName();

    if(fil == "")
        fil = tr("new");

    QString filename = QFileDialog::getSaveFileName(
                           this,
                           PROG_NAME " " VERSION " " + tr("Save file to pdf"),
                           path + fil + ".pdf",
                           "pdf files (*.pdf)");
    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(current));
    QPdfWriter pdfWriter(filename);
    pdfWriter.setTitle(fil);
    QString s = pTextEdit->toPlainText();
    auto *pteTmp = new QPlainTextEdit(s);
    pteTmp->setWordWrapMode(QTextOption::WordWrap);
    pteTmp->print(&pdfWriter);
    delete pteTmp;
}
void MainWindow::printDirect()
{
    const int current = ui->tabWidget->currentIndex();

    if(current < 0)
        return;

    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(current));

    if(QPrinterInfo::defaultPrinter().isNull()) {
        print();
        return;
    }

    QPrinter printer(QPrinterInfo::defaultPrinter(), QPrinter::ScreenResolution); // Linux and Windows
    // QPrinter printer(QPrinterInfo::defaultPrinter(), QPrinter::HighResolution); // Linux
    // printer.setOutputFormat(QPrinter::PdfFormat); // Linux
    printer.setOutputFormat(QPrinter::NativeFormat);  // Windows and Linux
    pTextEdit->print(&printer);
}
void MainWindow::print()
{
    const int current = ui->tabWidget->currentIndex();
    /* qDebug() << "List of printers";
     QList<QPrinterInfo> printerList = QPrinterInfo::availablePrinters();

     foreach(QPrinterInfo printerInfo, printerList) {
         qDebug() << printerInfo.printerName();
     }*/

    if(current < 0)
        return;

    // PDF
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    int index = settings.value("savePathType", 1).toInt();
    settings.endGroup();
    QString path;

    if(index != 0 && index != 9) {
        path = Tools::findPath(index);
    } else {
        path = settings.value("savePath").toString();
    }

    const QFileInfo outputDir(path);

    if((!outputDir.exists()) || (!outputDir.isDir())) {
        int fallback_path_index = settings.value("saveFallbackPathType", 1).toInt(); // i Home

        if(fallback_path_index == 0)
            path = Tools::findPath(0);
        else
            path = Tools::findPath(fallback_path_index);
    }

    QFileInfo fi(tab.at(current));
    QString fil = fi.fileName();

    if(fil == "")
        fil = tr("new");

    // SLUT PDF
    //QPrinter printer(QPrinter::HighResolution); // Linux
    QPrinter printer(QPrinter::ScreenResolution);  // Windows and Linux
#ifdef Q_OS_LINUX
    printer.setOutputFileName(path + fil + ".pdf"); // Linux
    printer.setOutputFormat(QPrinter::PdfFormat); // Linux
#endif // Q_OS_LINUX
#ifdef Q_OS_WIN
    printer.setOutputFormat(QPrinter::NativeFormat);  // Windows
#endif // Q_OS_WIN
    QPrintDialog dialog(&printer);
    dialog.setWindowTitle(PROG_NAME + tr(" Print Document"));
    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(current));

    if(pTextEdit->textCursor().hasSelection())
        dialog.addEnabledOption(QAbstractPrintDialog::PrintSelection);

    if(dialog.exec() != QDialog::Accepted) {
        return;
    }

    // qDebug() << "printer valid:" << printer.isValid();
    // qDebug() << "printer state:" << printer.printerState();
    pTextEdit->print(&printer);
}
void MainWindow::ny()
{
    tab.append(""); // Adding an empty string (filename)
    modified.append(false); // // Adding an "false" post
    const int count = ui->tabWidget->count();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("WordWrap");
    QString wordwrap = settings.value("wordwrap", "No").toString();
    settings.endGroup();
    auto *plainedit = new QPlainTextEdit;

    if(wordwrap == "No")
        plainedit->setWordWrapMode(QTextOption::NoWrap);
    else
        plainedit->setWordWrapMode(QTextOption::WordWrap);

    settings.beginGroup("Font");
    const QString ceditFont = settings.value("font", MONOFACE_FONT).toString();
    const int fontsize = settings.value("fontSize", DEFAULT_FONT_SIZE).toInt();
    settings.endGroup();
    QFont font(ceditFont);
    font.setPointSize(fontsize);
    plainedit->setFont(font);
    const int a = ui->tabWidget->insertTab(count, plainedit, tr("New "));
    ui->tabWidget->setCurrentIndex(a);
    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(a));
    connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
        textChanged();
    });
}
// QFileDialog
void MainWindow::open()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    int index = settings.value("openPathType", 1).toInt();;
    QString path;

    if(index != 0 && index != 9) {
        path = Tools::findPath(index);
    } else {
        path = settings.value("openPath").toString();
    }

    const QFileInfo outputDir(path);

    if((!outputDir.exists()) || (!outputDir.isDir())) {
        int fallback_path_index = settings.value("openFallbackPathType", 1).toInt(); // 1 Home

        if(fallback_path_index == 0)
            path = Tools::findPath(8);
        else
            path = Tools::findPath(fallback_path_index);
    }

    settings.endGroup();
    QStringList filenames =  QFileDialog::getOpenFileNames(
                                 this,
                                 PROG_NAME " " VERSION " " + tr("Open file/files"),
                                 path,
                                 "All files (*)");
    int a = 0;
    settings.beginGroup("WordWrap");
    QString wordwrap = settings.value("wordwrap", "No").toString();
    settings.endGroup();

    for(int i = 0; i < filenames.size(); i++) {
        if(tab.contains(filenames.at(i))) {
            continue;
        }

        if(!filenames.at(i).isEmpty()) {
            if(index == 0) {  // Last used
                QFileInfo direcory_path(filenames.at(i));
                QString path = direcory_path.canonicalPath();
                settings.beginGroup("ToolsWindow");
                settings.setValue("openPath", path);
                settings.endGroup();
            }
        } else {
            continue;
        }

        int count = ui->tabWidget->count();
        QFile inputFile(QDir::toNativeSeparators(filenames.at(i)));
        auto *plainedit = new QPlainTextEdit;

        if(wordwrap == "No")
            plainedit->setWordWrapMode(QTextOption::NoWrap);
        else
            plainedit->setWordWrapMode(QTextOption::WordWrap);

        settings.beginGroup("Font");
        const QString ceditFont = settings.value("font", MONOFACE_FONT).toString();
        const int fontsize = settings.value("fontSize", DEFAULT_FONT_SIZE).toInt();
        settings.endGroup();
        QFont font(ceditFont);
        font.setPointSize(fontsize);
        plainedit->setFont(font);
        a = ui->tabWidget->insertTab(count, plainedit, tr("New ") + QString::number(count));
        inputFile.open(QIODevice::Text | QIODevice::ReadOnly);
        QString content = "";

        while(!inputFile.atEnd())
            content.append(inputFile.readLine());

        plainedit->setPlainText(content);
        inputFile.close();
        QFileInfo fi(filenames.at(i));
        QString fil = fi.fileName();
        ui->tabWidget->setTabText(count, fil);
        ui->tabWidget->setTabToolTip(count, filenames.at(i));
        modified.append(false);
        tab.append(filenames.at(i));
    }

    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(a));
    connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
        textChanged();
    });
    ui->tabWidget->setCurrentIndex(a);
    // ui->tabWidget->tabBarClicked(a);
}
// Drag and Drop
void MainWindow::open(const QStringList * filenames)
{
    // qDebug() << *filenames;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Font");
    const QString ceditFont = settings.value("font", MONOFACE_FONT).toString();
    const int fontsize = settings.value("fontSize", DEFAULT_FONT_SIZE).toInt();
    settings.endGroup();
    settings.beginGroup("WordWrap");
    QString wordwrap = settings.value("wordwrap", "No").toString();
    settings.endGroup();
    int a = 0;

    for(int i = 0; i < filenames->size(); i++) {
        if(tab.contains(filenames->at(i))) {
            continue;
        }

        modified.append(false);
        //   tab.append("");
        tab.append(filenames->at(i));
        // int currentIndex = ui->tabWidget->currentIndex();
        int count = ui->tabWidget->count();
        QFile inputFile(QDir::toNativeSeparators(filenames->at(i)));
        auto *plainedit = new QPlainTextEdit;

        if(wordwrap == "No")
            plainedit->setWordWrapMode(QTextOption::NoWrap);
        else
            plainedit->setWordWrapMode(QTextOption::WordWrap);

        QFont font(ceditFont);
        font.setPointSize(fontsize);
        plainedit->setFont(font);
        a = ui->tabWidget->insertTab(count, plainedit, tr("New ") + QString::number(count));
        inputFile.open(QIODevice::Text | QIODevice::ReadOnly);
        QString content = "";

        while(!inputFile.atEnd()) {
            content.append(inputFile.readLine());
        }

        plainedit->setPlainText(content);
        inputFile.close();
        QFileInfo fi(filenames->at(i));
        QString fil = fi.fileName();
        ui->tabWidget->setTabText(count, fil);
        ui->tabWidget->setTabToolTip(count, filenames->at(i));
        emit ui->tabWidget->tabBarClicked(count);
        modified.append(false);
    }

    auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(a));
    connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
        textChanged();
    });
    ui->tabWidget->setCurrentIndex(a);
}
bool MainWindow::save()
{
    int tabIndex = ui->tabWidget->currentIndex();

    if(tabIndex < 0)
        return false;

    QString filename = tab.at(tabIndex);
    filename = QDir::toNativeSeparators(filename);

    if(filename.isEmpty()) {
        return saveAs();
    }

    QFile outputFile(filename);

    if(!outputFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox::critical(this, PROG_NAME " " VERSION, tr("Unable to save file, check your file permissions! ") + outputFile.errorString());
    } else {
        int current = ui->tabWidget->currentIndex();
        auto *plainedit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(current));
        QString text = plainedit->toPlainText();
        QTextStream out(&outputFile);
        out << text;
        outputFile.close();
        modified[tabIndex] = false;
        ui->tabWidget->tabBar()->setTabTextColor(tabIndex, Qt::black);
    }

    return true;
}
bool MainWindow::saveAs()
{
    int tabIndex = ui->tabWidget->currentIndex();

    if(tabIndex < 0)
        return false;

    auto *plainedit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(tabIndex));
    QString text = plainedit->toPlainText();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("ToolsWindow");
    int index = settings.value("savePathType", 1).toInt();
    QString path;

    if(index != 0 && index != 9) {
        path = Tools::findPath(index);
    } else {
        path = settings.value("savePath").toString();
    }

    const QFileInfo outputDir(path);

    if((!outputDir.exists()) || (!outputDir.isDir())) {
        int fallback_path_index = settings.value("saveFallbackPathType", 1).toInt(); // i Home

        if(fallback_path_index == 0)
            path = Tools::findPath(0);
        else
            path = Tools::findPath(fallback_path_index);
    }

    settings.endGroup();
    // QFileDialog *savefile = new QFileDialog(this);
    // savefile->setFileMode(QFileDialog::AnyFile);
    // savefile->setAcceptMode(QFileDialog::AcceptSave);
    // savefile->setViewMode(QFileDialog::Detail);
    // QString filename = savefile->getSaveFileName(
    QString filename = QFileDialog::getSaveFileName(
                           this,
                           PROG_NAME " " VERSION " " + tr("Save file/files"),
                           path,
                           "All files (*)");

    // Kola om filen redan finns, om så stänga gamla fliken
    if(filename.isEmpty() && filename.isNull())
        return false;

    int tabort = tab.indexOf(filename);

    if(tabort != tabIndex) {
        if(tabort >= 0) {
            tab.removeAt(tabort);
            ui->tabWidget->removeTab(tabort);
        }
    }

    tabIndex = ui->tabWidget->currentIndex();

    if(filename.isEmpty()) {
        return false;
    }

    if(index == 0) {  // Last used
        QFileInfo direcory_path(filename);
        QString path = direcory_path.canonicalPath();
        settings.beginGroup("ToolsWindow");
        settings.setValue("savePath", path);
        settings.endGroup();
    }

    QFile outputFile(filename);

    if(!outputFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox::critical(this, PROG_NAME " " VERSION, tr("Unable to save file, check your file permissions! ") + outputFile.errorString());
    } else {
        QTextStream out(&outputFile);
        out << text;
        outputFile.close();
        tab.replace(tabIndex, filename);
        ui->tabWidget->setCurrentIndex(tabIndex);
        QFileInfo fi(filename);
        QString fil = fi.fileName();
        ui->tabWidget->setTabText(tabIndex, fil);
        ui->tabWidget->setTabToolTip(tabIndex, filename);
        modified[tabIndex] = false;
        ui->tabWidget->tabBar()->setTabTextColor(tabIndex, Qt::black);
    }

    return true;
}
// View
void MainWindow::wordWrap()
{
    const int currentTabIndex = ui->tabWidget->currentIndex();

    if(currentTabIndex >= 0) {
        auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(currentTabIndex));
        pTextEdit->setWordWrapMode(QTextOption::WordWrap);
    }
}
void MainWindow::noWordWrap()
{
    const int currentTabIndex = ui->tabWidget->currentIndex();

    if(currentTabIndex >= 0) {
        auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(currentTabIndex));
        pTextEdit->setWordWrapMode(QTextOption::NoWrap);
    }
}
// Edit
void MainWindow::redo()
{
    const int currentTabIndex = ui->tabWidget->currentIndex();

    if(currentTabIndex >= 0) {
        auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(currentTabIndex));
        pTextEdit->redo();
    }
}
void MainWindow::undo()
{
    const int currentTabIndex = ui->tabWidget->currentIndex();

    if(currentTabIndex >= 0) {
        auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(currentTabIndex));
        pTextEdit->undo();
    }
}
void MainWindow::copyPath()
{
    const int index = ui->tabWidget->currentIndex();

    if(index >= 0) {
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(QDir::toNativeSeparators(tab.at(index)));
    }
}
// Help
void MainWindow::about()
{
    auto *s = new System;
    const QString v = s->getSystem();
    QMessageBox::about(this, PROG_NAME " " VERSION, v);
    delete s;
}
void MainWindow::update()
{
#ifdef Q_OS_LINUX
    QString *updateinstructions = new QString(tr("Select \"Tools\", \"Update\" to update."));
#endif
#ifdef Q_OS_WIN
#ifdef portable
    QString *updateinstructions = new QString(tr("Download a new") + " <a href=\"" DOWNLOAD_PATH "\"> portable</a>");
#endif
    QString *updateinstructions = new QString(tr("Select \"Tools\", \"Maintenance Tool\" and \"Update component\"."));
#endif
    auto *c = new CheckForUpdates;
    c->check(PROG_NAME, VERSION, VERSION_PATH, *updateinstructions);
}
void MainWindow::versionhistory()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "en_US").toString();
    settings.endGroup();
    v = new Visa;
    QString fil = ":/txt/version_history." + value;

    if(! v->visaFil(&fil)) {
        delete v;
        QMessageBox::critical(this, PROG_NAME " " VERSION, tr("Unable to find \"") + fil + "\"");
    } else
        v->show();
}
void MainWindow::license()
{
    v = new Visa;
    QString fil = ":/txt/license.txt";

    if(! v->visaFil(&fil)) {
        delete v;
        QMessageBox::critical(this, PROG_NAME " " VERSION, tr("Unable to find \"") + fil + "\"");
    } else
        v->show();
}
// Language
void MainWindow::english()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "nothing").toString();

    if(value != "en_US") {
        switch(QMessageBox::information(this, PROG_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.setValue("lang", "en_US");
                bool exit = setEndConfig();

                if(exit) {
                    return;
                }

                // QApplication::quit();
                startaom();
        }
    }

    settings.endGroup();
}

void MainWindow::swedish()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "nothing").toString();

    if(value != "sv_SE") {
        switch(QMessageBox::information(this, PROG_NAME " " VERSION,
                                        tr("The program must be restarted for the new language settings to take effect."),
                                        tr("Restart Now"),
                                        tr("Cancel"), nullptr, 2)) {
            case 0:
                settings.setValue("lang", "sv_SE");
                bool exit = setEndConfig();

                if(exit) {
                    return;
                }

                // QApplication::quit();
                startaom();
        }
    }

    settings.endGroup();
}

void MainWindow::translate()
{
    QDesktopServices::openUrl(QUrl(TRANSLATE));
}
// Tools
void MainWindow::updateProgramstart()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");

    if(!ui->actionUpdateProgramstart->isChecked()) {
        settings.beginGroup("Tools");
        settings.setValue("UpdateAtStart", 0);
        settings.endGroup();
    }

    if(ui->actionUpdateProgramstart->isChecked()) {
        settings.beginGroup("Tools");
        settings.setValue("UpdateAtStart", 1);
        settings.endGroup();
    }
}
void MainWindow::removeSettings()
{
    switch(QMessageBox::critical(this, PROG_NAME " " VERSION,
                                 tr("All program settings will be deleted. The program will shut down."),
                                 tr("Delete all settings"),
                                 tr("Cancel"), nullptr, 2)) {
        case 0:
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
            settings.setIniCodec("UTF-8");
            QString s = settings.fileName();
            int i = s.lastIndexOf(QChar('/'));
            s = s.left(i);
            QDir dir(s);
            dir.removeRecursively();
            exit(EXIT_FAILURE);
    }
}
void MainWindow::settings()
{
    settingsTools = new Tools;
    settingsTools->show();
}
void MainWindow::helpOnline()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "en_US").toString();
    settings.endGroup();

    if(value == "sv_SE") {
        QDesktopServices::openUrl(QUrl(APPLICATION_HELPPAGE));
    } else {
        QDesktopServices::openUrl(QUrl(APPLICATION_HELPPAGE_ENG));
    }
}
void MainWindow::helpOffline()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString value = settings.value("lang", "en_US").toString();
    settings.endGroup();
    QString path = "";
    const QString EXEKUTEDIR = QDir::toNativeSeparators(QApplication::applicationDirPath() + "/");

    if(value == "sv_SE") {
        path =   EXEKUTEDIR + "help/help.html";
    } else {
        path =  EXEKUTEDIR + "help/help_eng.html";
    }

    path = QDir::toNativeSeparators(path);
    QUrl url = QUrl::fromLocalFile(path);
    QDesktopServices::openUrl(url);
}
// tab clicked
void MainWindow::tabClicked(int index)
{
    if(index >= 0) {
        ui->tabWidget->setTabToolTip(index, tab.at(index));
// ipac 2018-08-22
        auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(index));
        connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
            textChanged();
        });
    }
}
// Text Changed
void MainWindow::textChanged()
{
    const int a = ui->tabWidget->currentIndex();
    modified[a] = true;
    ui->tabWidget->tabBar()->setTabTextColor(a, Qt::red);
}
// tab close
void MainWindow::closeRequested(int index)
{
    // ipac
    emit ui->tabWidget->tabBarClicked(index);

    if(modified.at(index)) {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle(PROG_NAME " " VERSION);
        msgBox.setText(tr("The document has been modified."));
        msgBox.setInformativeText(tr("Do you want to save your changes?"));
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();

        switch(ret) {
            case QMessageBox::Save:

                // Save was clicked
                if(!save())
                    return;

                break;

            case QMessageBox::Discard:
                // Don't Save was clicked
                break;

            case QMessageBox::Cancel:
                // Cancel was clicked
                return;
        }
    }

    ui->tabWidget->setTabToolTip(index, tab.at(index));
    //
    ui->tabWidget->removeTab(index);
    tab.removeAt(index);
    modified.removeAt(index);
    // ipac
    /*    if(index > 0) {
            const int a = index - 1;
            ui->tabWidget->setCurrentIndex(a);
            auto *pTextEdit = dynamic_cast<QPlainTextEdit*>(ui->tabWidget->widget(a));
            connect(pTextEdit, &QPlainTextEdit::textChanged, [this]() {
                textChanged();
            });

    }*/
}
