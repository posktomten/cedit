//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "visa.h"
#include "mainwindow.h"
#include "ui_visa.h"

Visa::Visa(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Visa)
{
    this->setAttribute(Qt::WA_DeleteOnClose, true);
    ui->setupUi(this);
    // Font
    /* QString fontPath = ":/fonts/fonts/Ubuntu-M.ttf";
     int id = QFontDatabase::addApplicationFont(fontPath);
     QString family = QFontDatabase::applicationFontFamilies(id).at(0);
     QFont default_font(family);
     default_font.setPointSize(DEFAULT_FONT_SIZE_SYSTEM);
     this->setFont(default_font);*/
    // End Font
    //  setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint); // Works on Windows, not Lubuntu Linux
    this->setWindowTitle(PROG_NAME " " VERSION);
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->virtualGeometry();
    QPoint screenSize(screenGeometry.width(), screenGeometry.height());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Visa");
    resize(settings.value("size", QSize(400, 400)).toSize());
    QPoint p = settings.value("pos", QPoint(100, 100)).toPoint();
    settings.endGroup();
    settings.beginGroup("MainWindow");
    QPoint  screenSavedSize = settings.value("screenSavedSize", QPoint(0, 0)).toPoint();
    settings.endGroup();

    if(screenSavedSize == screenSize)
        this->move(p);
    else
        this->move(100, 100);

    connect(ui->pbClose, &QPushButton::clicked, [this] { close(); });
}

Visa::~Visa()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, PROG_NAME, PROG_NAME);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Visa");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
    delete ui;
}
// private slots:

// public:
bool Visa::visaFil(const QString *infil)
{
//    const QString install_dir = QDir::toNativeSeparators(QApplication::applicationDirPath() + "/");
//    const QString s = install_dir + *infil;
    QFile inputFile(*infil);
    //ui->tbVisa->setAcceptRichText(true);

    if(inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        in.setCodec("UTF-8");

        while(!in.atEnd()) {
            ui->tbVisa->append(in.readLine());
        }

        inputFile.close();
        QTextCursor cursor = ui->tbVisa->textCursor();
        cursor.setPosition(0);
        ui->tbVisa->setTextCursor(cursor);
        return true;
    }

    return false;
}
