//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          CEdit
//          Copyright (C) 2018 - 2020 Ingemar Ceicer
//          https://gitlab.com/posktomten/cedit
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QCloseEvent>
#include <QPrintDialog>
#include <QPrinter>
#include <QPrinterInfo>
#include <QPagedPaintDevice>
#include <QPdfWriter>
#include <QDesktopWidget>

/* draganddrop */
#include <QDropEvent>
#include "system.h"
#include "visa.h"
#include "tools.h"
#include "checkforupdates.h"
#include "stable.h"
#include "updatedialog.h"
#include "update.h"

#define PROG_NAME "CEdit"
#define VERSION "0.0.4"
#define CREATION_YEAR "2018"
#define VERSION_PATH "http://bin.ceicer.com/cedit/version.txt"
#define DOWNLOAD_PATH "https://gitlab.com/posktomten/cedit/-/wikis/Download-%5BBETA-Continuous-program-development%5D"
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "ic_0002@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE "https://gitlab.com/posktomten/cedit/-/wikis/home"
#define APPLICATION_HOMEPAGE_ENG "https://gitlab.com/posktomten/cedit/-/wikis/home"
#define APPLICATION_HELPPAGE "http://bin.ceicer.com/cedit/help/index.php"
#define APPLICATION_HELPPAGE_ENG "http://bin.ceicer.com/cedit/help/index_eng.php"
#define TRANSLATE "https://bin.ceicer.com/down/translate/instructions.php"
#define HOMEPAGE "http://ceicer.org"
#define FONT_SIZE_MAXIMUM 38
#define FONT_SIZE_MINIMUM 6
#define DEFAULT_FONT_SIZE 11
// #define DEFAULT_FONT_SIZE_SYSTEM 8
#define BUILD_DATE_TIME __DATE__ " " __TIME__

#if Q_PROCESSOR_WORDSIZE == 4
#define ZSYNC_ARG_1 "http://bin.ceicer.com/zsync/cedit-i386.AppImage.zsync"
#define ZSYNC_ARG_2 "-i cedit-i386.AppImage"
#else
#define ZSYNC_ARG_1 "http://bin.ceicer.com/zsync/cedit-x86_64.AppImage.zsync"
#define ZSYNC_ARG_2 "-i cedit-x86_64.AppImage"
#endif

#ifdef Q_OS_WIN
#define MONOFACE_FONT "Courier"
#define FONT_SIZE 11
#endif // Q_OS_WIN
#ifdef Q_OS_LINUX
#define MONOFACE_FONT "Monospace"
#define FONT_SIZE 13
#endif // Q_OS_LINUX


/*
 * Ändra också "system.cpp", "const QString BESKRIVNING"
 * const QString BESKRIVNING = tr("A program that is a template for a real program.");
*/

namespace Ui
{
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


protected:
    Visa *v;
    Tools *settingsTools;
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);

private:
    Ui::MainWindow *ui;
    void setStartConfig();
    bool setEndConfig();
    bool setEndConfig(QCloseEvent *event);
    void startaom();

    void open();
    void open(const QStringList *filenames);
    bool save();
    bool saveAs();
    void closeEvent(QCloseEvent *event);

    QStringList tab;
    QList<bool> modified;

    UpdateDialog *ud;




private slots:

    void about();
    void update();
    void english();
    void swedish();
    void translate();
    void updateProgramstart();
    void license();
    void versionhistory();
    void removeSettings();
    void settings();
    void helpOnline();
    void helpOffline();
    void ny();
    void tabClicked(int index);
    void closeRequested(int index);
    void textChanged();
    void copyPath();
    void redo();
    void undo();
    void wordWrap();
    void noWordWrap();
    void print();
    void printDirect();
    void printPDF();
    void closeTab();

public slots:
    void isUpdated(bool uppdated);


};
#endif // MAINWINDOW_H
